from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

import pytest

from ansible_collections.spbstu.swordfish.plugins.module_utils.rest_client import RESTClient
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_client import SwordfishAPI
from ansible_collections.spbstu.swordfish.tests.unit.compat.mock import MagicMock
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.constants import \
    OPEN_URL_FUNC


@pytest.fixture
def swordfish():
    return SwordfishAPI(
        base_url='localhost',
        username='Administrator',
        password='Password'
    )


@pytest.fixture
def rest():
    return RESTClient(
        base_url='localhost',
        username='Administrator',
        password='Password'
    )


@pytest.fixture
def make_mock(mocker):
    def f(target, return_value=None, side_effect=None, chain_calls=False):
        if return_value is None:
            return_value = {}

        if target == OPEN_URL_FUNC:
            return_value = _mock_open_url(return_value, chain_calls)

        mock = mocker.patch(
            target,
            side_effect=side_effect,
            return_value=return_value,
        )

        return mock

    def _mock_open_url(return_value, chain_calls):
        response_mock = MagicMock()
        if chain_calls and return_value is not None:
            gen = (json.dumps(d) for d in return_value)
            response_mock.read = gen.__next__ \
                if hasattr(gen, '__next__') else gen.next
        else:
            response_mock.read.return_value = json.dumps(return_value)
        return response_mock

    return f


@pytest.fixture
def open_url_kwargs():
    return {
        "method": 'GET',
        "url": "https://localhost",
        "data": None,
        "validate_certs": False,
        "use_proxy": True,
        "timeout": 60,
        "follow_redirects": "all",
        "headers": {},
        "force_basic_auth": False,
    }


@pytest.fixture()
def storage_collection():
    return {
        "@odata.id": "/redfish/v1/Storage",
        "@odata.type": "#StorageCollection.StorageCollection",
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
            }
        ],
        "Members@odata.count": 2,
        "Name": "Storage Collection"
    }


@pytest.fixture()
def storage_data():
    return {
        "@odata.type": "#Storage.v1_13_0.Storage",
        "Id": "1",
        "Name": "NVMe IP Attached Drive Configuration",
        "Description": "An NVM Express Subsystem is an NVMe device that contains one or more NVM Express controllers"
                       " and may contain one or more namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK",
            "HealthRollup": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159f50245"
            }
        ],
        "Controllers": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers"
        },
        "Drives": [
            {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
            }
        ],
        "Volumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
        },
        "Links": {
            "Enclosures": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
                }
            ]
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved.",
        "StoragePool": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools"
        }
    }


@pytest.fixture()
def storage_pools_collection_data():
    return {
        "@Redfish.Copyright": "Copyright 2014-2021 SNIA. All rights reserved.",
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools",
        "@odata.type": "#StoragePoolCollection.StoragePoolCollection",
        "Members": [
            {"@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/TestPool"}
        ],
        "Members@odata.count": 1,
        "Name": "StoragePool Collection"
    }


@pytest.fixture
def drives_groups_data():
    return {
        "@odata.type": "#Storage.v1_13_0.Storage",
        "Id": "1",
        "Name": "NVMe IP Attached Drive Configuration",
        "Description": "An NVM Express Subsystem is an NVMe device that contains one or more NVM Express controllers"
                       " and may contain one or more namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK",
            "HealthRollup": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159f50245"
            }
        ],
        "Controllers": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers"
        },
        "Drives": [
            {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
            }
        ],
        "Volumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
        },
        "Links": {
            "Enclosures": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
                }
            ]
        },
        "StoragePool": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools"
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
    }


@pytest.fixture
def storage_pools_data():
    return [
        {
            "@odata.type": "#StoragePool.v1_8_0.StoragePool",
            "Id": "SimpleSystemPool",
            "Name": "Simple System Storage Pool",
            "Description": "Simple Storage Pool for Simple System",
            "SupportedRAIDTypes": [
                "RAID0",
                "RAID5"
            ],
            "RemainingCapacityPercent": 0,
            "Capacity": {
                "Data": {
                    "AllocatedBytes": 600135780272,
                    "ConsumedBytes": 600135780272
                }
            },
            "ReplicationEnabled": True,
            "CapacitySources": [
                {
                    "@odata.type": "#Capacity.v1_2_0.CapacitySource",
                    "Id": "Source1",
                    "Name": "Source1",
                    "ProvidedCapacity": {
                        "Data": {
                            "ConsumedBytes": 300067890136,
                            "AllocatedBytes": 600135780272
                        }
                    },
                    "ProvidingDrives": {
                        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/StoragePool1/"
                                     "CapacitySources/Source1/ProvidingDrives"
                    },
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/StoragePool1/CapacitySources/"
                                 "Source1"
                }
            ],
            "AllocatedVolumes": {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/StoragePool1/AllocatedVolumes"
            },
            "Links": {
                "OwningStorageResource": {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
                }
            },
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/TestPool",
            "@Redfish.Copyright": "Copyright 2015-2023 SNIA. All rights reserved."
        }
    ]


@pytest.fixture
def storage_pool_data():
    return {
        "@odata.type": "#StoragePool.v1_8_0.StoragePool",
        "Id": "SimpleSystemPool",
        "Name": "Simple System Storage Pool",
        "Description": "Simple Storage Pool for Simple System",
        "SupportedRAIDTypes": [
            "RAID0",
            "RAID5"
        ],
        "RemainingCapacityPercent": 0,
        "Capacity": {
            "Data": {
                "AllocatedBytes": 600135780272,
                "ConsumedBytes": 600135780272
            }
        },
        "ReplicationEnabled": True,
        "CapacitySources": [
            {
                "@odata.type": "#Capacity.v1_2_0.CapacitySource",
                "Id": "Source1",
                "Name": "Source1",
                "ProvidedCapacity": {
                    "Data": {
                        "ConsumedBytes": 300067890136,
                        "AllocatedBytes": 600135780272
                    }
                },
                "ProvidingDrives": {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/StoragePool1/"
                                 "CapacitySources/Source1/ProvidingDrives"
                },
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/StoragePool1/CapacitySources/"
                             "Source1"
            }
        ],
        "AllocatedVolumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/StoragePool1/AllocatedVolumes"
        },
        "Links": {
            "OwningStorageResource": {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
            }
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/TestPool",
        "@Redfish.Copyright": "Copyright 2015-2023 SNIA. All rights reserved."
    }


@pytest.fixture
def drive_data():
    return {
        "@odata.type": "#Drive.v1_15_0.Drive",
        "Name": "NVMe IPAttachedDrive Drive",
        "Id": "IPAttachedDrive",
        "BlockSizeBytes": 512,
        "CapableSpeedGbs": 12,
        "CapacityBytes": 899527000000,
        "Description": "IP Attached drive.",
        "EncryptionAbility": "None",
        "FailurePredicted": False,
        "Identifiers": [
            {
                "DurableNameFormat": "NAA",
                "DurableName": "500003942810D13A"
            }
        ],
        "LocationIndicatorActive": True,
        "MediaType": "SSD",
        "Manufacturer": "Contoso",
        "Model": "ST9146802SS",
        "NegotiatedSpeedGbs": 12,
        "PartNumber": "SG0GP8811253178M02GJA00",
        "PhysicalLocation": {
            "PartLocation": {
                "LocationType": "Slot"
            }
        },
        "PredictedMediaLifeLeftPercent": 86,
        "Protocol": "NVMe",
        "Revision": "S20A",
        "SKU": "N/A",
        "SerialNumber": "72D0A037FRD26",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "StatusIndicator": "OK",
        "WriteCacheEnabled": True,
        "Links": {
            "Volumes": [
                {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
                }
            ],
            "NetworkDeviceFunctions": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/NetworkAdapters/8fd725a1/"
                                 "NetworkDeviceFunctions/11100"
                },
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/NetworkAdapters/8fd725a1/"
                                 "NetworkDeviceFunctions/11101"
                }
            ]
        },
        "Actions": {
            "#Drive.Reset": {
                "target": "/redfish/v1/Chassis/IPAttachedDrive/Drives/IPAttachedDrive1/Actions/Drive.Reset"
            },
            "#Drive.SecureErase": {
                "target": "/redfish/v1/Chassis/IPAttachedDrive/Drives/IPAttachedDrive1/Actions/Drive.SecureErase"
            }
        },
        "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
    }


@pytest.fixture()
def storage_volume_collection():
    return {
        "@odata.type": "#VolumeCollection.VolumeCollection",
        "Name": "Storage Volume Collection",
        "Description": "Storage Volume Collection",
        "Members@odata.count": 3,
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
            }
        ],
        "Oem": {},
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
    }


@pytest.fixture()
def volume_data():
    return {
        "@odata.type": "#Volume.v1_8_0.Volume",
        "Id": "1",
        "Name": "Namespace 1",
        "LogicalUnitNumber": 1,
        "Description": "A Namespace is a quantity of non-volatile memory that may be formatted into logical blocks."
                       " When formatted, a namespace of size n is a collection of logical blocks with logical block"
                       " addresses from 0 to (n-1). NVMe systems can support multiple namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159029384"
            }
        ],
        "Capacity": {
            "Data": {
                "ConsumedBytes": 0,
                "AllocatedBytes": 10737418240
            }
        },
        "NVMeNamespaceProperties": {
            "NamespaceId": "0x011",
            "NamespaceFeatures": {
                "SupportsThinProvisioning": False,
                "SupportsAtomicTransactionSize": False,
                "SupportsDeallocatedOrUnwrittenLBError": False,
                "SupportsNGUIDReuse": False,
                "SupportsIOPerformanceHints": False
            },
            "NumberLBAFormats": 0,
            "FormattedLBASize": "LBAFormat0Support",
            "MetadataTransferredAtEndOfDataLBA": False,
            "NVMeVersion": "1.4"
        },
        "OptimumIOSizeBytes": 1024,
        "Links": {},
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
    }


@pytest.fixture()
def storage_ip_attached_drive1():
    return {
        "@odata.type": "#Storage.v1_13_0.Storage",
        "Id": "1",
        "Name": "NVMe IP Attached Drive Configuration",
        "Description": "An NVM Express Subsystem is an NVMe device that contains one or more NVM Express controllers"
                       " and may contain one or more namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK",
            "HealthRollup": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159f50245"
            }
        ],
        "Controllers": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers"
        },
        "Drives": [
            {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure/Drives/IPAttachedDrive1"
            }
        ],
        "Volumes": {
            "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes"
        },
        "Links": {
            "Enclosures": [
                {
                    "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
                }
            ]
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1"
    }


@pytest.fixture
def capacity_source_data():
    return {
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/storage_pool/CapacitySources/capacity_source",
        "@odata.type": "#Capacity.vstorage_pool_capacity_source_IPAttachedDrive1.CapacitySource",
        "Id": "capacity_source",
        "Name": "CapacitySource",
        "Description": "some description",
    }


@pytest.fixture()
def capacity_sources_collection_data():
    return {
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/storage_pool/CapacitySources",
        "@odata.type": "#CapacityCollection.CapacityCollection",
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/StoragePools/storage_pool/CapacitySources/capacity_source"
            }
        ],
        "Members@odata.count": 1,
        "Name": "Capacity Collection"
    }


@pytest.fixture()
def namespace_data():
    return {
        "@odata.type": "#Volume.v1_8_0.Volume",
        "Id": "SimpleNamespace",
        "Name": "SimpleNamespace",
        "LogicalUnitNumber": 1,
        "Description": "A Namespace is a quantity of non-volatile memory that may be formatted"
                       " into logical blocks. When formatted, a namespace of size n is a collection"
                       " of logical blocks with logical block addresses from 0 to (n-1). NVMe systems"
                       " can support multiple namespaces.",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "Identifiers": [
            {
                "DurableNameFormat": "NQN",
                "DurableName": "nqn.2014-08.org.nvmexpress:uuid:6c5fe566-10e6-4fb6-aad4-8b4159029384"
            }
        ],
        "Capacity": {
            "Data": {
                "ConsumedBytes": 0,
                "AllocatedBytes": 10737418240
            }
        },
        "NVMeNamespaceProperties": {
            "NamespaceId": "0x011",
            "NamespaceFeatures": {
                "SupportsThinProvisioning": False,
                "SupportsAtomicTransactionSize": False,
                "SupportsDeallocatedOrUnwrittenLBError": False,
                "SupportsNGUIDReuse": False,
                "SupportsIOPerformanceHints": False
            },
            "NumberLBAFormats": 0,
            "FormattedLBASize": "LBAFormat0Support",
            "MetadataTransferredAtEndOfDataLBA": False,
            "NVMeVersion": "1.4"
        },
        "OptimumIOSizeBytes": 1024,
        "Links": {},
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved."
    }


@pytest.fixture()
def storage_controller_data():
    return {
        "@odata.type": "#StorageController.v1_6_0.StorageController",
        "Id": "NVMeIOController",
        "Name": "NVMe I/O Controller",
        "Description": "An NVM IO controller is a general-purpose controller that provides access to logical block data"
                       " and metadata stored on an NVM subsystem’s non-volatile storage medium. IO Controllers may also"
                       " support management capabilities.",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "Manufacturer": "Best NVMe Vendor",
        "Model": "Simple NVMe Device",
        "SerialNumber": "NVME123456",
        "PartNumber": "NVM44",
        "FirmwareVersion": "1.0.0",
        "SupportedControllerProtocols": [
            "NVMeOverFabrics",
            "RoCEv2"
        ],
        "SupportedDeviceProtocols": [
            "NVMe"
        ],
        "NVMeControllerProperties": {
            "ControllerType": "IO",
            "NVMeVersion": "1.4",
            "NVMeControllerAttributes": {
                "ReportsUUIDList": False,
                "SupportsSQAssociations": False,
                "ReportsNamespaceGranularity": False,
                "SupportsTrafficBasedKeepAlive": False,
                "SupportsPredictableLatencyMode": False,
                "SupportsEnduranceGroups": False,
                "SupportsReadRecoveryLevels": False,
                "SupportsNVMSets": False,
                "SupportsExceedingPowerOfNonOperationalState": False,
                "Supports128BitHostId": False
            },
            "NVMeSMARTCriticalWarnings": {
                "MediaInReadOnly": False,
                "OverallSubsystemDegraded": False,
                "SpareCapacityWornOut": False
            },
            "MaxQueueSize": 1
        },
        "Links": {
            "AttachedVolumes": [
                {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
                },
                {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace1"
                }
            ]
        },
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers/NVMeIOController"
    }


@pytest.fixture()
def connection_data():
    return {
        "@odata.type": "#Connection.v1_0_0.Connection",
        "@Redfish.ReleaseStatus": "WorkInProgress",
        "Id": "1",
        "Name": "Connection zone 1",
        "Description": "Connection info for zone 1",
        "ConnectionType": "Storage",
        "VolumeInfo": [
            {
                "AccessCapabilities": [
                    "Read"
                ],
                "Volume": {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
                }
            },
            {
                "AccessCapabilities": [
                    "Read"
                ],
                "Volume": {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive2/Volumes/SimpleNamespace"
                }
            }
        ],
        "Links": {
            "InitiatorEndpoints": [
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Endpoints/Initiator1"
                },
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Endpoints/Initiator2"
                }
            ],
            "TargetEndpoints": [
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Endpoints/D1-E1"
                },
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Endpoints/D1-E2"
                }
            ],
            "TargetEndpointGroups": [
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/EndpointGroups/TargetEPs"
                }
            ],
            "InitiatorEndpointGroups": [
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/EndpointGroups/1"
                }
            ]
        },
        "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Connections/1",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved."
    }


@pytest.fixture()
def fabric_data():
    return {
        "@odata.type": "#Fabric.v1_2_1.Fabric",
        "@Redfish.ReleaseStatus": "WorkInProgress",
        "Id": "NVMeoF",
        "Name": "NVMe-oF Fabric",
        "Description": "NVMeoF Fabric for EBOF",
        "FabricType": "NVMeOverFabrics",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "Endpoints": {
            "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Endpoints"
        },
        "EndpointGroups": {
            "@odata.id": "/redfish/v1/Fabrics/NVMeoF/EndpointGroups"
        },
        "Connections": {
            "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Connections"
        },
        "Links": {
            "Oem": {}
        },
        "@odata.id": "/redfish/v1/Fabrics/NVMeoF",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved."
    }


@pytest.fixture()
def manager_data():
    return {
        "@odata.type": "#Manager.v1_15_0.Manager",
        "Id": "EBOF-SoC",
        "Name": "EBOF-SoC Manager",
        "ManagerType": "EnclosureManager",
        "Description": "Contoso EBOF-SoC",
        "ServiceEntryPointUUID": "92384634-2938-2342-8820-489239905423",
        "UUID": "58893887-8974-2487-2389-841168418919",
        "Manufacturer": "Box Manufacturer",
        "Model": "Joo Janta 200",
        "DateTime": "2021-03-13T04:14:33+06:00",
        "DateTimeLocalOffset": "+06:00",
        "SerialNumber": "AX1823-045",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "PowerState": "On",
        "CommandShell": {
            "ServiceEnabled": True,
            "MaxConcurrentSessions": 4,
            "ConnectTypesSupported": [
                "Telnet",
                "SSH"
            ]
        },
        "FirmwareVersion": "1.00",
        "GraphicalConsole": {
            "ConnectTypesSupported": [],
            "MaxConcurrentSessions": 1,
            "ServiceEnabled": False,
        },
        "SerialConsole": {
            "ConnectTypesSupported": [],
            "MaxConcurrentSessions": 1,
            "ServiceEnabled": False,
        },
        "NetworkProtocol": {
            "@odata.id": "/redfish/v1/Managers/EBOF-SoC/NetworkProtocol"
        },
        "EthernetInterfaces": {
            "@odata.id": "/redfish/v1/Managers/EBOF-SoC/EthernetInterfaces"
        },
        "LocationIndicatorActive": False,
        "LogServices": {
            "@odata.id": "/redfish/v1/Managers/EBOF-SoC/LogServices"
        },
        "Links": {
            "ManagerForChassis": [{
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
            }],
            "ManagerInChassis": {
                "@odata.id": "/redfish/v1/Chassis/EBOFEnclosure"
            },
            "Oem": {}
        },
        "Actions": {
            "#Manager.Reset": {
                "target": "/redfish/v1/Managers/EBOF-SoC/Actions/Manager.Reset",
                "ResetType@Redfish.AllowableValues": [
                    "ForceOff", "PowerCycle", "On"
                ]
            },
            "Oem": {}
        },
        "Oem": {},
        "@odata.id": "/redfish/v1/Managers/EBOF-SoC",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved."
    }


@pytest.fixture()
def network_protocol_data():
    return {
        "@odata.type": "#ManagerNetworkProtocol.v1_8_1.ManagerNetworkProtocol",
        "Id": "NetworkProtocol",
        "Name": "Manager Network Protocol",
        "Description": "Manager Network Service",
        "Status": {
            "State": "Enabled",
            "Health": "OK"
        },
        "HostName": "ebofmgr1",
        "FQDN": "ebofmgr1.dmtf.org",
        "HTTP": {
            "ProtocolEnabled": True,
            "Port": 80
        },
        "HTTPS": {
            "ProtocolEnabled": True,
            "Port": 443
        },
        "SSH": {
            "ProtocolEnabled": True,
            "Port": 22
        },
        "SSDP": {
            "ProtocolEnabled": True,
            "Port": 1900,
            "NotifyMulticastIntervalSeconds": 600,
            "NotifyTTL": 5,
            "NotifyIPv6Scope": "Site"
        },
        "Telnet": {
            "ProtocolEnabled": True,
            "Port": 23
        },
        "Oem": {},
        "@odata.id": "/redfish/v1/Managers/EBOF-SoC/NetworkProtocol",
        "NTP": {
            "NTPServers": [],
            "ProtocolEnabled": False
        }
    }


@pytest.fixture
def volume_collection_data():
    return {
        "@odata.type": "#VolumeCollection.VolumeCollection",
        "Name": "Storage Volume Collection",
        "Description": "Storage Volume Collection",
        "Members@odata.count": 1,
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive2/Volumes/SimpleNamespace"
            }
        ],
        "Oem": {},
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive2/Volumes",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved."
    }


@pytest.fixture
def storage_controller_collection_data():
    return {
        "@odata.type": "#StorageControllerCollection.StorageControllerCollection",
        "Name": "Storage Controller Collection",
        "Description": "Storage Controller Collection",
        "Members@odata.count": 1,
        "Members": [
            {
                "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers/NVMeIOController"
            }
        ],
        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Controllers",
        "@Redfish.Copyright": "Copyright 2015-2022 SNIA. All rights reserved."
    }
