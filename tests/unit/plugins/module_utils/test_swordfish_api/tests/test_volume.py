from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base_collection import SwordfishAPICollection
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestVolume:
    def test_get_volume(self, rest, make_mock, volume_data, volume_collection_data):
        make_mock(target=OPEN_URL_FUNC, return_value=volume_data)
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)
        volume_collection._supported_type = Volume
        volume = volume_collection.get_resource(resource_id="SimpleNamespace")
        assert isinstance(volume, Volume)
        expected_volume = Volume.from_json(client=rest, data=volume_data)
        assert volume.name == expected_volume.name
        assert volume.id == expected_volume.id
        assert volume.status == expected_volume.status
        assert volume.description == expected_volume.description

    def test_get_volume_not_found(self, rest, make_mock, volume_collection_data):
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)
        make_mock(
            target=OPEN_URL_FUNC,
            side_effect=RESTClientError,
        )
        with pytest.raises(RESTClientError):
            volume_collection.get_resource("SimpleNamespace-fake")

    def test_create_volume(self, rest, make_mock, open_url_kwargs, volume_collection_data, volume_data):
        open_url_mock = make_mock(target=OPEN_URL_FUNC)
        create_volume_id = "test-volume"
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)
        path = "{0}/{1}".format(volume_collection.path, create_volume_id)

        volume_collection.create_resource(resource_id=create_volume_id, data=volume_data)
        json_post_data = json.dumps(volume_data)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_post_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_create_volume_with_body(self, rest, make_mock, volume_collection_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC)
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)
        volume_id = "test-volume"
        path = "{0}/{1}".format(volume_collection.path, volume_id)

        body_data = {"Manufacturer": "NVMeDriveVendorFoo"}
        json_patch_data = json.dumps(body_data)
        volume_collection.create_resource(resource_id=volume_id, data=body_data)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_delete_volume(self, rest, make_mock, volume_collection_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC)
        delete_volume_id = "test-volume"
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)
        path = "{0}/{1}".format(volume_collection.path, delete_volume_id)

        volume_collection.delete_resource(resource_id=delete_volume_id)
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}".format(path),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_patch_volume(self, rest, make_mock, volume_collection_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC)
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)
        volume_id = "test-volume"
        path = "{0}/{1}".format(volume_collection.path, volume_id)

        patch_data = {"Manufacturer": "NVMeDriveVendorFoo"}
        json_patch_data = json.dumps(patch_data)
        volume_collection.patch_resource(resource_id=volume_id, data=patch_data)
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
