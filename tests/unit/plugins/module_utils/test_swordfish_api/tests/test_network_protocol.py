from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestNetworkProtocol:
    def test_get_network_protocol(self, make_mock, swordfish, manager_data, network_protocol_data):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[manager_data, network_protocol_data],
            chain_calls=True
        )

        manager = swordfish.get_manager("EBOF-SoC")
        npd = manager.network_protocol
        npd_expected = network_protocol_data

        assert npd.id == npd_expected.get('Id')
        assert npd.name == npd_expected.get('Name')
        assert npd.path == npd_expected.get('@odata.id')
        assert npd.description == npd_expected.get('Description')
        assert npd.hostname == npd_expected.get('HostName')
        assert npd.ntp_enabled == npd_expected.get('NTP').get('ProtocolEnabled')
        assert len(npd.ntp_servers) == len(npd_expected.get('NTP').get('NTPServers'))
        assert npd.ssh_enabled == npd_expected.get('SSH').get('ProtocolEnabled')
        assert npd.ntp == npd_expected.get('NTP')
