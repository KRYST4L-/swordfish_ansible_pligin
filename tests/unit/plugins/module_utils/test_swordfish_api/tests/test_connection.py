from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.connection import Connection
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestConnection:

    def test_connection(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")
        expected_connection = Connection.from_json(swordfish, connection_data)
        assert connection.name == expected_connection.name
        assert connection.id == expected_connection.id
        assert connection.description == expected_connection.description
        assert connection.links == expected_connection.links
        assert connection.initiator_endpoint_groups == expected_connection.initiator_endpoint_groups
        assert connection.target_endpoint_groups == expected_connection.target_endpoint_groups
        assert connection.initiator_endpoints == expected_connection.initiator_endpoints
        assert connection.target_endpoints == expected_connection.target_endpoints
        assert connection.volume_info == expected_connection.volume_info
        assert connection.connection_type == expected_connection.connection_type

    def test_connection_set_name(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs

    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data, connection_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")

        expected_new_name = "2"
        connection.name = expected_new_name
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(connection.path),
            data=json.dumps({"Name": expected_new_name}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_any_call(**open_url_kwargs)

    def test_connection_set_description(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data, connection_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")

        expected_new_description = "Description"
        connection.description = expected_new_description
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(connection.path),
            data=json.dumps({"Description": expected_new_description}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_any_call(**open_url_kwargs)

    def test_connection_set_links(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data, connection_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")

        expected_new_links = {
            "InitiatorEndpointGroups": [
                {
                    "@odata.id": "/redfish/v1/Fabrics/NVMeoF/EndpointGroups/1"
                }
            ],
            "TargetEndpointGroups": []
        }
        connection.links = expected_new_links
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(connection.path),
            data=json.dumps({"Links": expected_new_links}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_any_call(**open_url_kwargs)

    def test_connection_set_volume_info(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data, connection_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")

        expected_new_volume_info = [
            {
                "AccessCapabilities": [
                    "Read"
                ],
                "Volume": {
                    "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
                }
            }
        ]
        connection.volume_info = expected_new_volume_info
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(connection.path),
            data=json.dumps({"VolumeInfo": expected_new_volume_info}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_any_call(**open_url_kwargs)

    def test_connection_set_connection_type(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data, connection_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")

        expected_new_connection_type = "Storage1"
        connection.connection_type = expected_new_connection_type
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(connection.path),
            data=json.dumps({"ConnectionType": expected_new_connection_type}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_any_call(**open_url_kwargs)
