from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestStoragePool:

    def test_get_storage_pools_from_storage(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_pools_data,
            storage_pools_collection_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pools_collection_data, storage_pools_data[0]],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")

        pools = storage.storage_pools
        storage_pool = pools[0]
        expected_storage_pool = StoragePool.from_json(swordfish, storage_pools_data[0])
        assert storage_pool.name == expected_storage_pool.name
        assert storage_pool.id == expected_storage_pool.id
        assert storage_pool.description == expected_storage_pool.description
        assert storage_pool.odata_id == expected_storage_pool.odata_id
        assert storage_pool.capacity == expected_storage_pool.capacity
        assert storage_pool.capacity_sources == expected_storage_pool.capacity_sources
        assert storage_pool.allocated_volumes == expected_storage_pool.allocated_volumes

    def test_get_storage_pool(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_pool_data,
            storage_pools_collection_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pool_data, storage_pools_collection_data, storage_pool_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")

        storage_pool = storage.get_storage_pool("TestPool")
        expected_storage_pool = StoragePool.from_json(swordfish, storage_pool_data)
        assert storage_pool.name == expected_storage_pool.name
        assert storage_pool.id == expected_storage_pool.id
        assert storage_pool.description == expected_storage_pool.description
        assert storage_pool.odata_id == expected_storage_pool.odata_id
        assert storage_pool.capacity == expected_storage_pool.capacity
        assert storage_pool.capacity_sources == expected_storage_pool.capacity_sources
        assert storage_pool.allocated_volumes == expected_storage_pool.allocated_volumes
