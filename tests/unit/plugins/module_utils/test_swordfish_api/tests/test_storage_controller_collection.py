from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestStorageControllerCollection:

    def test_storage_controller_collection_patch_controller(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_controller_collection_data,
            storage_controller_data,
            open_url_kwargs,
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_controller_collection_data, storage_controller_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_controller_collection = storage.get_storage_controller_collection("Controllers")
        storage_controller_collection.patch_resource(resource_id=storage_controller_data["Name"],
                                                     data=storage_controller_data)
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}/{1}".format(storage_controller_collection.path,
                                                  storage_controller_data["Name"]),
            data=json.dumps(storage_controller_data),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_storage_controller_collection_create_controller(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_controller_collection_data,
            storage_controller_data,
            open_url_kwargs,
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_controller_collection_data, storage_controller_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_controller_collection = storage.get_storage_controller_collection("Controllers")
        storage_controller_collection.create_resource(resource_id=storage_controller_data["Name"],
                                                      data=storage_controller_data)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/{1}".format(storage_controller_collection.path,
                                                  storage_controller_data["Name"]),
            data=json.dumps(storage_controller_data),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_storage_controller_collection_delete_controller(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_controller_collection_data,
            storage_controller_data,
            open_url_kwargs,
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_controller_collection_data, storage_controller_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_controller_collection = storage.get_storage_controller_collection("Controllers")
        storage_controller_collection.delete_resource(resource_id=storage_controller_data["Name"])
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}/{1}".format(storage_controller_collection.path,
                                                  storage_controller_data["Name"]),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_storage_controller_collection_get_controller(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_controller_collection_data,
            storage_controller_data,
            open_url_kwargs,
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_controller_collection_data, storage_controller_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_controller_collection = storage.get_storage_controller_collection("Controllers")
        storage_controller_collection.get_resource(resource_id=storage_controller_data["Name"])
        open_url_kwargs.update(
            method='GET',
            url="https://localhost{0}/{1}".format(storage_controller_collection.path,
                                                  storage_controller_data["Name"]),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
