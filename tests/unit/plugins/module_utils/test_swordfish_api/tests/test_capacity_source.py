from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base_collection import SwordfishAPICollection
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestCapacitySource:
    def test_create_capacity_source(self, rest, make_mock, capacity_source_data, volume_collection_data,
                                    open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=capacity_source_data)
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)

        volume_id = "SimpleNamespace"
        capacity_source_id = "test-cap"
        body_data = {"Description": "some-description"}

        volume = volume_collection.get_resource(volume_id)
        volume.create_capacity_source(capacity_source_id=capacity_source_id, data=body_data)

        json_data = json.dumps(body_data)
        path = "{0}/CapacitySources/{1}".format(volume.path, capacity_source_id)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_delete_capacity_source(self, rest, make_mock, capacity_source_data, volume_collection_data,
                                    open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=capacity_source_data)
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)

        volume_id = "SimpleNamespace"
        capacity_source_id = "test-cap"

        volume = volume_collection.get_resource(volume_id)
        volume.delete_capacity_source(capacity_source_id)

        path = "{0}/CapacitySources/{1}".format(volume.path, capacity_source_id)
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}".format(path),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_patch_capacity_source(self, rest, make_mock, capacity_source_data, volume_collection_data,
                                   open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=capacity_source_data)
        volume_collection = SwordfishAPICollection.from_json(rest, volume_collection_data, Volume)

        volume_id = "SimpleNamespace"
        capacity_source_id = "test-cap"

        volume = volume_collection.get_resource(volume_id)

        patch_data = {"Description": "some-description"}
        volume.patch_capacity_source(capacity_source_id=capacity_source_id, data=patch_data)

        path = "{0}/CapacitySources/{1}".format(volume.path, capacity_source_id)
        json_patch_data = json.dumps(patch_data)
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
