from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import pytest
from ansible_collections.spbstu.swordfish.plugins.module_utils.rest_client import RESTClient
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.drive_group import DriveGroup
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.constants import \
    OPEN_URL_FUNC
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestDrives:

    def test_get_drive_group(
            self,
            swordfish,
            make_mock,
            storage_collection,
            drives_groups_data,
            drive_data,
            storage_volume_collection,
            volume_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_collection, drives_groups_data, drive_data, storage_volume_collection, volume_data],
            chain_calls=True
        )
        drive_group = swordfish.get_drive_groups()[0]

        assert drive_group.name == 'NVMe IP Attached Drive Configuration'
        assert drive_group.status == 'OK'
        assert drive_group.capacity_total == 10737418240
        assert drive_group.capacity_used == 0
        assert drive_group.capacity_available == 10737418240
        assert drive_group.drive_capacity == 899527000000
        assert drive_group.drives_total == 1
        assert drive_group.drives_available == 1
        assert drive_group.drives_failed == 0

        drive = drive_group.drives[0]

        assert drive.model == 'ST9146802SS'
        assert drive.serial_number == '72D0A037FRD26'
        assert drive.status == 'OK'
        assert drive.capacity == 899527000000
        assert drive.slot == 'Slot'
