from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.namespace import Namespace


class TestNamespace:

    def test_namespace(
            self,
            swordfish,
            make_mock,
            storage_data,
            namespace_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, namespace_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        namespace = storage.get_namespace('SimpleNamespace')
        expected_namespace = Namespace.from_json(swordfish, namespace_data)
        assert namespace.capacity == expected_namespace.capacity
