from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.manager.manager import Manager


class TestManager:

    def test_manager(
            self,
            swordfish,
            make_mock,
            manager_data,
            open_url_kwargs
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[manager_data],
            chain_calls=True
        )
        manager = swordfish.get_manager("EBOF-SoC")
        expected_manager = Manager.from_json(swordfish, manager_data)

        assert manager.firmware_version == expected_manager.firmware_version
        assert manager.status == expected_manager.status
        assert manager.service_entry_point_uuid == expected_manager.service_entry_point_uuid
        assert manager.uuid == expected_manager.uuid
        assert manager.power_state == expected_manager.power_state
        assert manager.graphical_console == expected_manager.graphical_console
        assert manager.serial_console == expected_manager.serial_console

    def test_manager_get_network_protocol(
            self,
            swordfish,
            make_mock,
            manager_data,
            network_protocol_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[manager_data, network_protocol_data],
            chain_calls=True
        )
        manager = swordfish.get_manager("EBOF-SoC")
        network_protocol = manager.network_protocol

        open_url_kwargs.update(
            method='GET',
            url="https://localhost{0}/NetworkProtocol".format(manager.path)
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_manager_reset_graceful(
            self,
            swordfish,
            make_mock,
            manager_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[manager_data, manager_data],
            chain_calls=True
        )
        manager = swordfish.get_manager("EBOF-SoC")
        manager.reset_graceful()

        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/Actions/Manager.Reset".format(manager.path),
            data=json.dumps({"ResetType": "GracefulRestart"}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_manager_reset_force(
            self,
            swordfish,
            make_mock,
            manager_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[manager_data, manager_data],
            chain_calls=True
        )
        manager = swordfish.get_manager("EBOF-SoC")
        manager.reset_force()

        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/Actions/Manager.Reset".format(manager.path),
            data=json.dumps({"ResetType": "ForceRestart"}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_manager_reset_to_defaults(
            self,
            swordfish,
            make_mock,
            manager_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[manager_data, manager_data],
            chain_calls=True
        )
        manager = swordfish.get_manager("EBOF-SoC")
        manager.reset_to_defaults("all")

        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/Actions/Manager.ResetToDefaults".format(manager.path),
            data=json.dumps({"ResetToDefaultsType": "ResetAll"}),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
