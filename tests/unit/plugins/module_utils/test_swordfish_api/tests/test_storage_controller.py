from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from unittest.mock import call

from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller import \
    StorageController


class TestStorageController:

    def test_get_storage_controller(
            self,
            swordfish,
            make_mock,
            storage_controller_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_controller_data],
            chain_calls=True
        )
        controller = swordfish.get_storage_controller("IPAttachedDrive1", "NVMeIOController")
        expected_controller = StorageController.from_json(swordfish, storage_controller_data)
        assert controller.name == expected_controller.name
        assert controller.id == expected_controller.id
        assert controller.description == expected_controller.description
        assert controller.status == expected_controller.status

        assert controller.manufacturer == expected_controller.manufacturer
        assert controller.model == expected_controller.model
        assert controller.serial_number == expected_controller.serial_number
        assert controller.part_number == expected_controller.part_number
        assert controller.firmware_version == expected_controller.firmware_version
        assert controller.supported_controller_protocols == expected_controller.supported_controller_protocols
        assert controller.supported_device_protocols == expected_controller.supported_device_protocols
        assert controller.nvme_controller_properties == expected_controller.nvme_controller_properties
        assert controller.links == expected_controller.links

    def test_attach_namespace(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            storage_controller_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_controller_data, storage_controller_data, storage_controller_data],
            chain_calls=True,
        )
        controller = swordfish.get_storage_controller("IPAttachedDrive1", "NVMeIOController")
        attaching_namespace = ['/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace2']
        controller.attach_namespace(attaching_namespace)

        expected_links = controller.links["AttachedVolumes"]
        expected_links.append({"@odata.id": attaching_namespace[0]})

        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(controller.path),
            headers={'Content-Type': 'application/json'},
            data=json.dumps({'Links': {
                "AttachedVolumes": expected_links
            }})
        )
        first_call = open_url_kwargs.copy()
        open_url_kwargs.update(
            method='GET',
            url="https://localhost{0}".format(controller.path),
            headers={},
            data=None
        )
        last = open_url_kwargs.copy()
        open_url_mock.assert_has_calls([call(**first_call), call(**last)])

    def test_detach_namespace(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            storage_controller_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_controller_data, storage_controller_data, storage_controller_data],
            chain_calls=True,
        )
        controller = swordfish.get_storage_controller("IPAttachedDrive1", "NVMeIOController")
        assert controller.manufacturer == 'Best NVMe Vendor'
        controller.detach_namespace(['/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace1'])
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(controller.path),
            headers={'Content-Type': 'application/json'},
            data=json.dumps({'Links': {
                'AttachedVolumes': [
                    {
                        "@odata.id": "/redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace"
                    }
                ]}})
        )
        first_call = open_url_kwargs.copy()
        open_url_kwargs.update(
            method='GET',
            url="https://localhost{0}".format(controller.path),
            headers={},
            data=None
        )
        last_call = open_url_kwargs.copy()
        open_url_mock.assert_has_calls([call(**first_call), call(**last_call)])
