from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base_collection import SwordfishAPICollection
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.namespace import Namespace
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller import \
    StorageController
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestStorage:

    def test_storage(
            self,
            swordfish,
            make_mock,
            storage_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        expected_storage = Storage.from_json(swordfish, storage_data)
        assert storage.name == expected_storage.name
        assert storage.id == storage.id
        assert storage.description == expected_storage.description

    def test_get_storage_pool(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_pool_data):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pool_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_pool = storage.get_storage_pool("TestPool")
        expected_storage_pool = StoragePool.from_json(swordfish, storage_pool_data)
        assert storage_pool.name == expected_storage_pool.name
        assert storage_pool.id == expected_storage_pool.id
        assert storage_pool.description == expected_storage_pool.description
        assert storage_pool.path == expected_storage_pool.path

    def test_get_storage_pools(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_pools_collection_data,
            storage_pools_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pools_collection_data, storage_pools_data[0]],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_pools = storage.storage_pools
        assert len(storage_pools) == 1
        storage_pool = storage_pools[0]
        expected_storage_pool = StoragePool.from_json(swordfish, storage_pools_data[0])
        assert storage_pool.name == expected_storage_pool.name
        assert storage_pool.id == expected_storage_pool.id
        assert storage_pool.description == expected_storage_pool.description
        assert storage_pool.path == expected_storage_pool.path

    def test_get_namespace(
            self,
            swordfish,
            make_mock,
            storage_data,
            namespace_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, namespace_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        namespace = storage.get_namespace('SimpleNamespace')
        expected_namespace = Namespace.from_json(swordfish, namespace_data)
        assert namespace.name == expected_namespace.name
        assert namespace.id == expected_namespace.id
        assert namespace.path == expected_namespace.path
        assert namespace.description == expected_namespace.description

    def test_delete_storage_pool(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            storage_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        storage.delete_storage_pool("TestPool")
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}/StoragePools/{1}".format(storage.path, "TestPool"),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_create_storage_pool(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            storage_data,
            storage_pool_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        storage.create_storage_pool(storage_pool_data, storage_pool_data["Name"])
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/StoragePools/{1}".format(storage.path, storage_pool_data["Name"]),
            headers={'Content-Type': 'application/json'},
            data=json.dumps(storage_pool_data)
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    # def test_storage_get_volume_collection(
    #         self,
    #         swordfish,
    #         make_mock,
    #         storage_data,
    #         volume_data,
    #         volume_collection_data,
    # ):
    #     make_mock(
    #         target=OPEN_URL_FUNC,
    #         return_value=[storage_data, volume_collection_data, volume_data, volume_data],
    #         chain_calls=True
    #     )
    #     storage = swordfish.get_storage("IPAttachedDrive1")
    #     volume_collection = storage.get_volume_collection("Volumes")
    #     expected_volume_collection = VolumeCollection.from_json(swordfish, volume_collection_data)
    #     volume_collection_members = volume_collection.members
    #     assert volume_collection.name == expected_volume_collection.name
    #     assert volume_collection_members == expected_volume_collection.members
    #     assert volume_collection.members_count == expected_volume_collection.members_count
    #     assert volume_collection.members_count == len(volume_collection_members)
    #     assert volume_collection.description == expected_volume_collection.description

    def test_storage_get_volume_collection(
            self,
            swordfish,
            make_mock,
            storage_data,
            volume_data,
            volume_collection_data,
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, volume_collection_data, volume_data, volume_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        volume_collection = storage.get_volume_collection("Volumes")
        expected_volume_collection = SwordfishAPICollection.from_json(swordfish, volume_collection_data, Volume)
        volume_collection_members = volume_collection.members
        assert volume_collection.name == expected_volume_collection.name
        assert volume_collection_members == expected_volume_collection.members
        assert volume_collection.members_count == expected_volume_collection.members_count
        assert volume_collection.members_count == len(volume_collection_members)
        assert volume_collection.description == expected_volume_collection.description

    def test_storage_get_storage_controller_collection(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_controller_collection_data,
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_controller_collection_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_controller_collection = storage.get_storage_controller_collection("Controllers")
        expected_storage_controller_collection = SwordfishAPICollection.from_json(
            swordfish,
            storage_controller_collection_data,
            StorageController
        )
        storage_controller_collection_members = storage_controller_collection.members
        assert storage_controller_collection_members == expected_storage_controller_collection.members
