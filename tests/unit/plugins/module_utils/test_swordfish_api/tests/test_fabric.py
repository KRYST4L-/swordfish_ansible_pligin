from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.fabric import Fabric
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestFabric:

    def test_fabric(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        expected_fabric = Fabric.from_json(swordfish, fabric_data)

        assert fabric.name == expected_fabric.name
        assert fabric.id == expected_fabric.id
        assert fabric.description == expected_fabric.description
        assert fabric.status == expected_fabric.status
        assert fabric.fabric_type == expected_fabric.fabric_type
        assert fabric.links == expected_fabric.links

    def test_fabric_get_connection(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        connection = fabric.get_connection("1")

        open_url_kwargs.update(
            method='GET',
            url="https://localhost{0}".format(connection.path)
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_fabric_create_connection(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        fabric.create_connection(connection_data, "test_connection")

        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/Connections/{1}".format(fabric.path, "test_connection"),
            data=json.dumps(connection_data),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_fabric_delete_connection(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        fabric.delete_connection("test_connection")

        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}/Connections/{1}".format(fabric.path, "test_connection")
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_fabric_patch_connection(
            self,
            swordfish,
            make_mock,
            fabric_data,
            connection_data,
            open_url_kwargs
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[fabric_data, connection_data],
            chain_calls=True
        )
        fabric = swordfish.get_fabric("NVMeoF")
        fabric.patch_connection(connection_data, "test_connection")

        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}/Connections/{1}".format(fabric.path, "test_connection"),
            data=json.dumps(connection_data),
            headers={'Content-Type': 'application/json'},
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
