# swordfish_ansible_plugin

# Download collection

```
ansible-galaxy collection install git+https://gitlab.com/IgorNikiforov/swordfish_ansible_plugin
```

## About the project

This project was created to develop an Ansible plugin to support the Swordfish specification for managing a data storage
system. The project is being developed by SPbPU students.

Существующие системы хранения данных в большинстве случаев предоставляют REST API для того, чтобы можно было ими управлять. В зависимости от производителя предоставляемый REST API может отличаться, что влечет за собой многообразие различных протоколов, команд и способов взаимодействия. Очевидно, что для потребителей, у которых установлены различные СХД, возникает потребность одновременной поддержки различных протоколов, что является трудоемким как с точки зрения разработки, так и поддержки (Рис. 1).

![alt text](https://i.ibb.co/QYB8bsZ/1.png)

Чтобы минимизировать количество разнообразных способов взаимодействия была разработана унифицированная спецификация Swordfish [1] на REST API, которой должны удовлетворять различные системы хранения данных (СХД). При этом решение о том, что конкретное СХД будет удовлетворять спецификации принимается производителем данной СХД (Рис. 2). 
![alt text](https://i.ibb.co/gV3SWNr/image.png)

Спецификация Swordfish является открытой и ее могут использовать любые производители СХД [2]. 

Системы хранения данных зачастую представляются не одним узлом (сервером), а целым набором серверов, количество которых при необходимости можно расширять или уменьшать (Рис.1 и Рис.2). Для того, чтобы быстро и эффективно конфигурировать разное количество узлов в множественном наборе, развернутом в облаке, используется инструмент Ansible [3]. В инструменте Ansible есть набор playbook, который позволяет выполнять различные команды из готового набора Ansible-модулей.  

Для спецификации Swordfish нет playbook и также нет Ansible-модулей. Этот недостаток влечет за собой невозможность быстрого и эффективного управления СХД, удовлетворяющего спецификации Swordfish, и как следствие каждому производителю СХД требуется самостоятельно реализовывать необходимые команды. 

Поэтому актуальной является задача разработка Ansible-модулей, которые могут использоваться в playbook, чтобы осуществлять управление СХД, которые удовлетворяют спецификации Swordfish.

## Coverage tables 

# Support of Swordfish Schema Types

| Schema                        | Condition          |
|-------------------------------|--------------------|
| CapacitySource                | :white_check_mark: |
| CapacitySourceCollection      | :question:         |
| ClassOfService                | :x:                |
| ClassOfServiceCollection      | :x:                |
| ConsistencyGroup              | :question:         |
| ConsistencyGroupCollection    | :x:                |
| DataProtectionLineOfService   | :x:                |
| DataProtectionLoSCapabilities | :x:                |
| DataSecurityLineOfService     | :x:                |
| DataSecurityLoSCapabilities   | :x:                |
| DataStorageLineOfService      | :x:                |
| DataStorageLoSCapabilities    | :x:                |
| FeaturesRegistry              | :x:                |
| FileShare                     | :question:         |
| FileShareCollection           | :x:                |
| FileSystem                    | :white_check_mark: |
| FileSystemCollection          | :x:                |
| HostedStorageServices         | :x:                |
| IOConnectivityLineOfService   | :x:                |
| IOConnectivityLoSCapabilities | :x:                |
| IOPerformanceLineOfService    | :x:                |
| IOPerformanceLoSCapabilities  | :x:                |
| LineOfService                 | :x:                |
| LineOfServiceCollection       | :x:                |
| NVMeDomain                    | :question:         |
| NVMeDomainCollection          | :x:                |
| NVMeFirmwareImage             | :x:                |
| SpareResourceSet              | :x:                |
| StorageGroup                  | :question:         |
| StorageGroupCollection        | :question:         |
| StoragePool                   | :white_check_mark: |
| StoragePoolCollection         | :x:                |
| StorageReplicaInfo            | :x:                |
| StorageService                | :question:         |
| StorageServiceCollection      | :x:                |
| StorageSystemCollection       | :x:                |
| Volume                        | :white_check_mark: |
| VolumeCollection              | :question:         |
| VolumeMetrics                 | :x:                |

# Support of Common Swordfish Objects

| Schema              | Condition          |
|---------------------|--------------------|
| Capacity            | :x:                |
| CapacityInfo        | :question:         |
| Identifier          | :x:                |
| IOStatistics        | :x:                |
| IOWorkload          | :x:                |
| IOWorkloadComponent | :x:                |
| Location            | :x:                |
| Oem                 | :x:                |
| ReplicaInfo         | :x:                |
| ReplicaRequest      | :x:                |
| Shedule             | :x:                |
| Status              | :x:                |

Table Legend:

- :white_check_mark: - implemented module
- :question: - in progress
- :x: - not implemented

# How to run modules

## Local import of collection (without ansible galaxy)

clone repo

add parents directories:

- ansible_collections
    - spbstu
        - swordfish
            - code from git clone

add collection to COLLECTIONS_PATH in config <br />
in ~ directory run <br />
```ansible-config init --disabled > .ansible.cfg``` <br />
add path to our collection in .ansible.cfg <br />
```collections_path=~/.ansible/collections:/usr/share/ansible/collections:/ur path/to/swordfish_ansible_pligin/ansible_collections``` <br />
run your playbook
```ansible-playbook test-playbook.yml```
example of playbook

```
---
- hosts: localhost
  tasks:
    - name: Execute my task
      spbstu.swordfish.swordfish_ping:
        connection:
          base_url: "https://127.0.0.1:5000"
          username: "Administrator"
          password: "Password"
      register: testout
    - name: dump test output
      debug:
        msg: '{{ testout }}'
```

to run with python and args

```
/path-to/swordfish_ansible_pligin$ export PYTHONPATH=$PYTHONPATH:/.
python3 ansible_collections/spbstu/swordfish/plugins/modules/swordfish_ping.py ansible_collections/spbstu/swordfish/tmp/args.json
```

# Running tests

do the previous steps then <br />
```path-to-ur-proj/swordfish_ansible_pligin/ansible_collections/spbstu/swordfish$ ansible-test integration```


# Running CI/CD tests locally
install docker
<br />
install gitlab runner
https://docs.gitlab.com/runner/install/linux-manually.html
<br />
cd in project directory
<br />
run
```
gitlab-runner exec docker pep8_tests
gitlab-runner exec docker unit_tests
gitlab-runner exec docker integration_tests
```

# Running Tests with Make

To run the tests locally, you need to have the following tools installed:

- Docker: Required for creating isolated environments and managing containers.
- Make: Used to simplify the process of running tests through a Makefile.

## Installing Docker

Visit the official [Docker website](https://docs.docker.com/get-docker/) and follow the instructions to install Docker on your operating system.

## Installing Make

The installation of `make` depends on your operating system. On UNIX-like systems (Linux, macOS), `make` is usually pre-installed or can be installed through a package manager.

For Debian/Ubuntu-based Linux users:
```bash
sudo apt-get install build-essential
```
## Running the Tests
After installing all necessary tools, you can execute the tests by running the following command in your terminal:
```bash
make test
```
This command will start the test procedures as defined in your Makefile.

To stop the tests, you can use the following keyboard shortcut:
```
Ctrl + C
```
