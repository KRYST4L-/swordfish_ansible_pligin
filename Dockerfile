FROM willhallonline/ansible:2.13-bullseye

WORKDIR /ansible_collections/spbstu/swordfish

ARG EMULATOR_URL
ENV EMULATOR_URL=${EMULATOR_URL}

RUN apt-get update && \
    apt-get install -y curl && \
    rm -rf /var/lib/apt/lists/*

RUN echo $EMULATOR_URL > ~/text.txt

COPY . .

CMD until curl -k --output /dev/null --silent --head --fail $EMULATOR_URL/redfish/v1/; do \
    echo "Waiting for emulator service..."; \
    sleep 5; \
    done; \
    echo "Emulator service is available."; \
    ansible-test sanity --test pep8; \
    ansible-test units --requirements; \
    ansible-test integration; \
