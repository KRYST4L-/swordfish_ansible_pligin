from __future__ import (absolute_import, division, print_function)

__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = r"""
    options:
      connection:
        required: True
        type: dict
        description: Used to specify connection options
        suboptions:
          base_url:
            required: True
            type: str
            description: Swordfish REST API entrypoint.
          username:
            type: str
            description: Swordfish username to login.
          password:
            type: str
            description: Swordfish user password.
"""
