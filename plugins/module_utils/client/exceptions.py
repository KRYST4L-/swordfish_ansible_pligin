class RedfishError(Exception):
    pass


class HTTPClientError(RedfishError):
    pass


class RESTClientUnauthorized(RedfishError):
    pass


class RESTClientRequestError(RedfishError):
    pass


class RESTClientNotFoundError(RedfishError):
    pass


class RESTClientConnectionError(RedfishError):
    pass
