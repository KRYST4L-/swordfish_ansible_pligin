from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Dict = None

import json
from ansible.module_utils.six.moves.http_client import HTTPResponse


class HTTPClientResponse:

    def __init__(self, response):  # type: (HTTPResponse) -> None
        self._response = response
        self._body = self._response.read()

    @property
    def json(self):  # type: () -> Dict
        try:
            return json.loads(self._body)
        except ValueError:
            raise ValueError("Unable to parse json")

    @property
    def headers(self):  # type: () -> Dict
        return HTTPClientResponse._http_message_to_dict_with_duplicates(self._response.headers)

    @staticmethod
    def _http_message_to_dict_with_duplicates(http_message):
        headers_dict = {}
        for key, value in http_message.items():
            if key in headers_dict:
                if not isinstance(headers_dict[key], list):
                    headers_dict[key] = [headers_dict[key]]
                headers_dict[key].append(value)
            else:
                headers_dict[key] = value
        return headers_dict

    @property
    def status_code(self):  # type: () -> int
        return self._response.getcode()

    @property
    def is_success(self):  # type: () -> bool
        return self.status_code in (200, 201, 202, 204)
