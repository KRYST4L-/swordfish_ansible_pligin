from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.client.response import HTTPClientResponse

try:
    from typing import Optional, List
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = List = None

from ansible.module_utils.urls import open_url
from ansible.module_utils.six.moves.urllib.parse import urlencode
from ansible.module_utils.six.moves.urllib.error import HTTPError
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import (
    RESTClientError,
    RESTAuthorizationError,
    RESTClientNotFoundError,
)
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import (
    SESSION_ENDPOINT,
)


class HTTPClient:
    def __init__(
            self,
            base_url,
            username=None,
            password=None,
            validate_certs=False,
            timeout=60,
    ):
        self._username = username
        self._password = password
        self._token = None
        self._session = None
        self._cookies = ""
        if "://" in base_url:
            self._protocol, self._host = base_url.split("://")
        else:
            self._protocol = "https"
            self._host = base_url
        self.validate_certs = validate_certs
        self.timeout = timeout

    def authorize(self, username=None, password=None, auth_method=None):
        self._username = username or self._username
        self._password = password or self._password

        try:
            headers = self.post(
                SESSION_ENDPOINT,
                body={'UserName': self._username, 'Password': self._password},
            ).headers

        except Exception as e:
            raise RESTAuthorizationError('{0}: {1}'.format(
                type(e).__name__, str(e)
            ))

        self._token = headers['X-Auth-Token']
        self._session = headers['Location']
        for cookie in headers['Set-Cookie']:
            self._cookies += cookie + ';'

    def _make_request(
            self,
            path,
            method,
            query_params=None,
            body=None,
            headers=None,
    ):
        # type: (str, str, str, Union[Dict, bytes], Dict) -> HTTPClientResponse

        request_kwargs = {
            "follow_redirects": "all",
            "force_basic_auth": False,
            "headers": self._get_headers(),
            "method": method,
            "timeout": self.timeout,
            "use_proxy": True,
            "validate_certs": self.validate_certs,
        }
        if body:
            if isinstance(body, dict) or isinstance(body, list):
                request_kwargs["headers"]["Content-Type"] = "application/json"
                request_body = json.dumps(body)
            elif isinstance(body, bytes):
                request_kwargs["headers"]["Content-Type"] = "application/octet-stream"
                request_body = body
            else:
                raise RESTClientError(
                    "Unsupported body type: {0}".format(type(body)))
        else:
            request_body = None

        url = "{0}/{1}".format(self.base_url.rstrip("/"), path.lstrip("/"))
        if query_params:
            url += "?" + urlencode(query_params)

        if headers:
            request_kwargs["headers"].update(headers)

        try:
            response = open_url(url=url, data=request_body, **request_kwargs)
        except HTTPError as e:
            if e.code == 404:
                raise RESTClientNotFoundError("{0} not found.".format(e.code))
            else:
                raise RESTClientError("Bad request: {0}".format(e.code))
        return HTTPClientResponse(response)

    def get(self, path, query_params=None, headers=None):  # type: (str, Dict, Dict) -> HTTPClientResponse
        return self._make_request(path, method="GET", query_params=query_params, headers=headers)

    def post(self, path, body=None, headers=None):  # type: (str, Dict, Dict) -> HTTPClientResponse
        return self._make_request(path, method="POST", body=body, headers=headers)

    def patch(self, path, body=None, headers=None):  # type: (str, Dict, Dict) -> HTTPClientResponse
        return self._make_request(path, method="PATCH", body=body, headers=headers)

    def delete(self, path, headers=None):  # type: (str, Dict, Dict) -> HTTPClientResponse
        return self._make_request(path, method="DELETE", headers=headers)

    def put(self, path, body=None, headers=None):  # type: (str, Dict, Dict) -> HTTPClientResponse
        return self._make_request(
            path, method="PUT", body=body, headers=headers)

    def _get_headers(self):
        headers = {}
        if self._token:
            headers['X-Auth-Token'] = self._token
            headers['Cookie'] = self._cookies
        return headers

    @property
    def base_url(self):
        return "{0}://{1}".format(self._protocol, self._host)

    def logout(self):  # type: () -> None
        if self._token:
            self.delete(self._session)
            self._token = None
            self._session = None
