from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError


class ManagerNetworkProtocol(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(ManagerNetworkProtocol, self).__init__(*args, **kwargs)

    @property
    def hostname(self):  # type: () -> str
        return self._get_field("HostName")

    @hostname.setter
    def hostname(self, hostname):  # type: (str) -> None
        if not isinstance(hostname, str):
            raise TypeError("HostName must be string. Received: {0}".format(type(hostname)))
        self._data["HostName"] = hostname
        self._client.patch(self._path, body={"HostName": self._data["HostName"]})
        self.reload()

    @property
    def ipmi_enabled(self):  # type: () -> bool
        try:
            return self._get_field("IPMI")["ProtocolEnabled"]
        except KeyError:
            raise SwordfishFieldNotFoundError(["IPMI"]["ProtocolEnabled"])

    @ipmi_enabled.setter
    def ipmi_enabled(self, enabled):  # type: (bool) -> None
        if not isinstance(enabled, bool):
            raise TypeError("Enabled must be boolean. Received: {0}".format(type(enabled)))
        if "IPMI" not in self._data:
            self._data["IPMI"] = dict()
        self._data["IPMI"]["ProtocolEnabled"] = enabled
        self._client.patch(self._path, body={"IPMI": self._data["IPMI"]})
        self.reload()

    @property
    def ntp_enabled(self):  # type: () -> bool
        try:
            return self._get_field("NTP")["ProtocolEnabled"]
        except KeyError:
            raise SwordfishFieldNotFoundError(["NTP"]["ProtocolEnabled"])

    @ntp_enabled.setter
    def ntp_enabled(self, enabled):  # type: (bool) -> None
        if not isinstance(enabled, bool):
            raise TypeError("Enabled must be boolean. Received: {0}".format(type(enabled)))
        if "NTP" not in self._data:
            self._data["NTP"] = dict()
        self._data["NTP"]["ProtocolEnabled"] = enabled
        self._client.patch(self._path, body={"NTP": self._data["NTP"]})
        self.reload()

    @property
    def ntp_servers(self):  # type: () -> List[str]
        try:
            return self._get_field("NTP")["NTPServers"]
        except KeyError:
            raise SwordfishFieldNotFoundError(["NTP"]["NTPServers"])

    @ntp_servers.setter
    def ntp_servers(self, ntp_servers):  # type: (List[str]) -> None
        if not isinstance(ntp_servers, list):
            raise TypeError("NTP servers must be list. Received: {0}".format(type(ntp_servers)))
        for server in ntp_servers:
            if not isinstance(server, str):
                raise TypeError("NTP server must be string. Received: {0}".format(type(server)))
        if "NTP" not in self._data:
            self._data["NTP"] = dict()
        self._data["NTP"]["NTPServers"] = ntp_servers
        self._client.patch(self._path, body={"NTP": self._data["NTP"]})
        self.reload()

    @property
    def ssh_enabled(self):  # type: () -> bool
        try:
            return self._get_field("SSH")["ProtocolEnabled"]
        except KeyError:
            raise SwordfishFieldNotFoundError(["SSH"]["ProtocolEnabled"])

    @ssh_enabled.setter
    def ssh_enabled(self, enabled):  # type: (bool) -> None
        if not isinstance(enabled, bool):
            raise TypeError("Enabled must be boolean. Received: {0}".format(type(enabled)))
        if "SSH" not in self._data:
            self._data["SSH"] = dict()
        self._data["SSH"]["ProtocolEnabled"] = enabled
        self._client.patch(self._path, body={"SSH": self._data["SSH"]})
        self.reload()

    @property
    def ntp(self):  # type: () -> dict
        return self._get_field("NTP")
