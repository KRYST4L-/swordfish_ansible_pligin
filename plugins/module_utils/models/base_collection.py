from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from typing import Optional

from ansible_collections.spbstu.swordfish.plugins.module_utils.client.exceptions import RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.client.response import HTTPClientResponse
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishModelLoadError, \
    SwordfishFieldNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class SwordfishAPICollection:
    def __init__(self, client, path, supported_resource, data=None):  # type: (any, str, dict, type) -> None
        self._client = client
        self._path = path.rstrip("/")
        self._supported_type = supported_resource
        if data:
            self._data = data
        else:
            self._data = self._client.get(self._path).json

    @classmethod
    def from_json(cls, client, data, supported_resource):  # type: (any, dict, type) -> SwordfishAPICollection
        if not isinstance(data, dict):
            raise TypeError("Data must be dictionary. Received: {0}".format(type(data)))
        if "@odata.id" not in data:
            raise SwordfishModelLoadError("Cannot identify object id from data.")
        if "@odata.type" not in data:
            raise SwordfishModelLoadError("Cannot identify object type from data.")
        return cls(client, data["@odata.id"], supported_resource, data)

    def _get_field(self, name):  # type: (str) -> Any
        try:
            return self._data[name]
        except KeyError:
            raise SwordfishFieldNotFoundError(name)

    @property
    def description(self):  # type: () -> Optional[dict]
        return self._get_field("Description")

    @description.setter
    def description(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New description must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Description": new})
        self.reload()

    @property
    def name(self):  # type: () -> Optional[str]
        return self._get_field("Name")

    @name.setter
    def name(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New name must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Name": new})
        self.reload()

    @property
    def members(self):  # type: () -> dict
        return self._get_field("Members")

    @property
    def path(self):  # type: () -> str
        return self._path

    def get_resource(self, resource_id):  # type: (str) -> Optional[SwordfishAPIObject]
        if not isinstance(resource_id, str):
            raise TypeError("element_id must be string. Received: {0}".format(type(resource_id)))
        try:
            data = self._client.get("{0}/{1}".format(self._path, resource_id)).json
            return self._supported_type.from_json(self._client, data)
        except RESTClientNotFoundError:
            return None

    def create_resource(self, data, resource_id):  # type: (dict, str) -> Optional[HTTPClientResponse]
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if not isinstance(resource_id, str):
            raise TypeError("element_id must be str. Received: {0}".format(type(resource_id)))
        path = "{0}/{1}".format(self._path, resource_id)
        return self._client.post(path, body=data)

    def patch_resource(self, data, resource_id):  # type: (dict, str) -> HTTPClientResponse
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if not isinstance(resource_id, str):
            raise TypeError("element_id must be str. Received: {0}".format(type(resource_id)))
        path = "{0}/{1}".format(self._path, resource_id)
        return self._client.patch(path, body=data)

    def delete_resource(self, resource_id):  # type: (dict) -> HTTPClientResponse
        if not isinstance(resource_id, str):
            raise TypeError("element_id must be str. Received: {0}".format(type(resource_id)))
        path = "{0}/{1}".format(self._path, resource_id)
        return self._client.delete(path)

    @property
    def members_count(self):  # type: () -> Optional[int]
        return self._get_field("Members@odata.count")
