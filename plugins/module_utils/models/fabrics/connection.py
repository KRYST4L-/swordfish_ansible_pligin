from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError


class Connection(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Connection, self).__init__(*args, **kwargs)

    @property
    def connection_type(self):  # type: () -> str
        return self._get_field("ConnectionType")

    @connection_type.setter
    def connection_type(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New connection type must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"ConnectionType": new})
        self.reload()

    @property
    def volume_info(self):  # type: () -> list
        return self._get_field("VolumeInfo")

    @volume_info.setter
    def volume_info(self, new):  # type: (list) -> None
        if not isinstance(new, list):
            raise TypeError("New volume_info must be list. Received: {0}".format(type(new)))
        for i in new:
            if not isinstance(i, dict):
                raise TypeError("Items of new volume_info must be dicts. Received: {0}".format(type(i)))
        self._client.patch(self._path, body={"VolumeInfo": new})
        self.reload()

    @property
    def links(self):  # type: () -> dict
        return self._get_field("Links")

    @links.setter
    def links(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New links must be dict. Received: {0}".format(type(new)))
        for i in new.items():
            if not isinstance(i[1], list):
                raise TypeError("Items of new links must be list. Received: {0}".format(type(i)))
        self._client.patch(self._path, body={"Links": new})
        self.reload()

    @property
    def initiator_endpoints(self):  # type: () -> list
        try:
            return self._data["Links"]["InitiatorEndpoints"]
        except KeyError:
            raise SwordfishFieldNotFoundError("[Links][InitiatorEndpoints]")

    @property
    def target_endpoints(self):  # type: () -> list
        try:
            return self._data["Links"]["TargetEndpoints"]
        except KeyError:
            raise SwordfishFieldNotFoundError("[Links][TargetEndpoints]")

    @property
    def target_endpoint_groups(self):  # type: () -> list
        try:
            return self._data["Links"]["TargetEndpointGroups"]
        except KeyError:
            raise SwordfishFieldNotFoundError("[Links][TargetEndpointGroups]")

    @property
    def initiator_endpoint_groups(self):  # type: () -> list
        try:
            return self._data["Links"]["InitiatorEndpointGroups"]
        except KeyError:
            raise SwordfishFieldNotFoundError("[Links][InitiatorEndpointGroups]")
