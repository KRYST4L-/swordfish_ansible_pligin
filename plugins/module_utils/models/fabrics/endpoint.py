from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Endpoint(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Endpoint, self).__init__(*args, **kwargs)

    @property
    def connected_entities(self):  # type: () -> dict
        return self._get_field("ConnectedEntities")

    @connected_entities.setter
    def connected_entities(self, new_value):  # type: (dict) -> None
        self._patch_field("ConnectedEntities", new_value)

    @property
    def endpoint_protocol(self):  # type: () -> str
        return self._get_field("EndpointProtocol")

    @endpoint_protocol.setter
    def endpoint_protocol(self, new_value):  # type: (str) -> None
        self._patch_field("EndpointProtocol", new_value)

    @property
    def host_reservation_memory_bytes(self):  # type: () -> int
        return self._get_field("HostReservationMemoryBytes")

    @host_reservation_memory_bytes.setter
    def host_reservation_memory_bytes(self, new_value):  # type: (int) -> None
        self._patch_field("HostReservationMemoryBytes", new_value)

    @property
    def identifiers(self):  # type: () -> list
        return self._get_field("Identifiers")

    @identifiers.setter
    def identifiers(self, new_value):  # type: (dict) -> None
        self._patch_field("Identifiers", new_value)

    @property
    def ip_transport_details(self):  # type: () -> list
        return self._get_field("IPTransportDetails")

    @ip_transport_details.setter
    def ip_transport_details(self, new_value):  # type: (dict) -> None
        self._patch_field("IPTransportDetails", new_value)

    @property
    def links(self):  # type: () -> dict
        return self._get_field("Links")

    @links.setter
    def links(self, new_value):  # type: (dict) -> None
        self._patch_field("Links", new_value)

    @property
    def pci_id(self):  # type: () -> dict
        return self._get_field("PciId")

    @pci_id.setter
    def pci_id(self, new_value):  # type: (dict) -> None
        self._patch_field("PciId", new_value)

    @property
    def redundancy(self):  # type: () -> list
        return self._get_field("Redundancy")

    @redundancy.setter
    def redundancy(self, new_value):  # type: (dict) -> None
        self._patch_field("Redundancy", new_value)

    @property
    def status(self):  # type: () -> dict
        return self._get_field("Status")

    @status.setter
    def status(self, new_value):  # type: (dict) -> None
        self._patch_field("Status", new_value)
