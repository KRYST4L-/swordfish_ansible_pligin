from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

try:
    from typing import ClassVar, Optional, Dict, Any
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    ClassVar = Optional = Dict = Any = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import (
    SwordfishFieldNotFoundError,
    SwordfishModelLoadError,
)


class SwordfishAPIObject:

    def __init__(self, client, path, data=None):  # type: (Any, str, Dict) -> None
        self._client = client
        self._path = path.rstrip("/")
        if data:
            self._data = data
        else:
            self._data = self._client.get(self._path).json

    def _get_field(self, name):  # type: (str) -> Any
        try:
            return self._data[name]
        except KeyError:
            raise SwordfishFieldNotFoundError(name)

    def _patch_field(self, name, value):  # type: (str, any) -> None
        if not isinstance(name, str):
            raise TypeError("{0} must be str. Received: {1}".format(name, type(name)))
        self._client.patch(self._path, body={name: value})
        self.reload()

    def contains_field(self, name):  # type: (str) -> bool
        if not isinstance(name, str):
            raise TypeError("Name must be str. Received: {0}".format(type(name)))
        return name in self._data

    def reload(self):  # type: () -> None
        self._data = self._client.get(self._path).json

    @property
    def id(self):  # type: () -> str
        return self._get_field("Id")

    @id.setter
    def id(self, new_id):  # type: (str) -> None
        self._patch_field("Id", new_id)

    @property
    def name(self):  # type: () -> str
        return self._get_field("Name")

    @name.setter
    def name(self, new_name):  # type: (str) -> None
        self._patch_field("Name", new_name)

    @property
    def path(self):  # type: () -> str
        return self._path

    @property
    def description(self):  # type: () -> str
        return self._get_field("Description")

    @description.setter
    def description(self, new_desc):  # type: (str) -> None
        self._patch_field("Description", new_desc)

    @classmethod
    def from_json(cls, client, data):  # type: (Any, Dict) -> SwordfishAPIObject
        if not isinstance(data, dict):
            raise TypeError("Data must be dictionary. Received: {0}".format(type(data)))
        if "@odata.id" not in data:
            raise SwordfishModelLoadError("Cannot identify object id from data.")
        if "@odata.type" not in data:
            raise SwordfishModelLoadError("Cannot identify object type from data.")
        return cls(client, data["@odata.id"], data)

    @classmethod
    def select_version(cls, version):  # type: (str) -> Optional[ClassVar[SwordfishAPIObject]]
        raise NotImplementedError("Method not implemented")

    def __repr__(self):  # type: () -> str
        return "{0}({1})".format(self.__class__.__name__, self._path)
