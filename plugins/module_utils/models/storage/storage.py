from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.client.response import HTTPClientResponse
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError, \
    RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base_collection import SwordfishAPICollection
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.file_system import FileSystem
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.namespace import Namespace
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller import \
    StorageController
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_POOLS_ENDPOINT, \
    FILE_SYSTEM_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool


class Storage(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Storage, self).__init__(*args, **kwargs)

    @property
    def storage_pools(self):
        pools = []
        if not self._data.get("StoragePool"):
            return pools

        storage_pools_data = self._client.get(STORAGE_POOLS_ENDPOINT
                                              .format(path_to_storage=self._path)).json['Members']
        for pool in storage_pools_data:
            pools.append(StoragePool(client=self._client, path=pool['@odata.id']))
        return pools

    def get_namespace(self, namespace_id):  # type: (str) -> Optional[Namespace]
        if not isinstance(namespace_id, str):
            raise TypeError("Namespace_id must be string. Received: {0}".format(type(namespace_id)))
        try:
            namespace_data = self._client.get("{0}/Volumes/{1}".format(self._path, namespace_id)).json
            return Namespace.from_json(self._client, namespace_data)
        except RESTClientNotFoundError:
            return None

    def patch_storage_pool(self, data, storage_pool_id):  # type: (dict, str) -> HTTPClientResponse
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be str. Received: {0}".format(type(storage_pool_id)))
        path = "{0}/StoragePools/{1}".format(self._path, storage_pool_id)
        return self._client.patch(path, body=data)

    def get_storage_pool(self, storage_pool_id):  # type: (str) -> Optional[StoragePool]
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        try:
            storage_pool_data = self._client.get("{0}/StoragePools/{1}".format(self._path, storage_pool_id)).json
            return StoragePool.from_json(self._client, storage_pool_data)
        except RESTClientNotFoundError:
            return None

    def delete_storage_pool(self, storage_pool_id):  # type: (str) -> HTTPClientResponse
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        path = "{0}/StoragePools/{1}".format(self._path, storage_pool_id)
        return self._client.delete(path)

    def create_storage_pool(self, data, storage_pool_id):  # type: (dict, str) -> HTTPClientResponse
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        path = "{0}/StoragePools/{1}".format(self._path, storage_pool_id)
        return self._client.post(path, body=data)

    @property
    def volumes(self):  # type: () -> List[Volume]
        volumes = []
        if not self._data.get("Volumes"):
            return volumes

        volumes_collection_data = self._client.get("{path}/Volumes"
                                                   .format(path=self._path)).json['Members']

        for volume in volumes_collection_data:
            volumes.append(Volume(client=self._client, path=volume['@odata.id']))
        return volumes_collection_data

    @property
    def volumes_info(self):  # type: () -> HTTPClientResponse
        volumes_collection_data = self._client.get("{path}/Volumes"
                                                   .format(path=self._path))
        return volumes_collection_data

    def get_volume_collection(self, volume_collection_id):  # type: (str) -> Optional[SwordfishAPICollection]
        if not isinstance(volume_collection_id, str):
            raise TypeError("Volume_collection_id must be string. Received: {0}".format(type(volume_collection_id)))
        try:
            volume_collection_data = self._client.get("{0}/{1}".format(self._path, volume_collection_id)).json
            return SwordfishAPICollection.from_json(self._client, volume_collection_data, Volume)
        except RESTClientNotFoundError:
            return None

    def get_file_system(self, file_system_id):  # type: (str) -> Optional[FileSystem]
        if not isinstance(file_system_id, str):
            raise TypeError("File_system_id must be string. Received: {0}".format(type(file_system_id)))
        try:
            file_system_data = self._client.get(
                FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=file_system_id)).json
            return FileSystem.from_json(self._client, file_system_data)
        except RESTClientNotFoundError:
            return None

    def delete_file_system(self, file_system_id):  # type: (str) -> None
        if not isinstance(file_system_id, str):
            raise TypeError("file_system_id must be string. Received: {0}".format(type(file_system_id)))
        path = FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=file_system_id)
        self._client.delete(path)

    def create_file_system(self, data):
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if data.get("Name") is None:
            raise SwordfishFieldNotFoundError("Name")
        path = FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=data["Name"])
        self._client.post(path, body=data)

    def patch_file_system(self, file_system_id, data):
        path = FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=file_system_id)
        return self._client.patch(path, body=data)

    def get_storage_controller_collection(
            self,
            storage_controller_collection_id):  # type: (str) -> Optional[SwordfishAPICollection]
        if not isinstance(storage_controller_collection_id, str):
            raise TypeError(
                "Storage_controller_collection_id must be string. Received: {0}".format(
                    type(storage_controller_collection_id)))
        try:
            storage_controller_collection = self._client.get(
                "{0}/{1}".format(self._path, storage_controller_collection_id)).json
            return SwordfishAPICollection.from_json(self._client, storage_controller_collection, StorageController)
        except RESTClientNotFoundError:
            return None
