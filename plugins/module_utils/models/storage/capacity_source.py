from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage import *

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class CapacitySource(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(CapacitySource, self).__init__(*args, **kwargs)

    @property
    def odata_id(self):  # type: () -> str
        return self._get_field("@odata.id")

    @property
    def actions(self):  # type: () -> list
        return self._get_field("Actions")

    def get_providing_volume(self, volume_id):
        path = "{0}/ProvidingVolumes/{1}".format(self.path, pool_id)
        volume = self._client.get(path).json
        return Volume.from_json(self._client, volume)

    def create_providing_volume(self, volume_id, data):
        path = "{0}/ProvidingVolumes/{1}".format(self.path, pool_id)
        self._client.post(path, body=data)

    def delete_providing_volume(self, volume_id):
        path = "{0}/ProvidingPools/{1}".format(self.path, pool_id)
        self._client.delete(path)

    def get_providing_pool(self, pool_id):
        path = "{0}/ProvidingPools/{1}".format(self.path, pool_id)
        pool = self._client.get(path).json
        return StoragePool.from_json(self._client, pool)

    def create_providing_pool(self, pool_id, data):
        path = "{0}/ProvidingPools/{1}".format(self.path, pool_id)
        self._client.post(path, body=data)

    def delete_providing_pool(self, pool_id):
        path = "{0}/ProvidingPools/{1}".format(self.path, pool_id)
        self._client.delete(path)
