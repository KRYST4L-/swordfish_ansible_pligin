from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Namespace(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Namespace, self).__init__(*args, **kwargs)

    @property
    def capacity(self):  # type: () -> Dict
        return self._get_field("Capacity")
