from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.capacity_source import CapacitySource

try:
    from typing import List, Optional
except ImportError:
    List = None


class FileSystem(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(FileSystem, self).__init__(*args, **kwargs)

    @property
    def access_capabilities(self):  # type: () -> list
        return self._get_field("AccessCapabilities")

    @property
    def actions(self):  # type: () -> list
        return self._get_field("Actions")

    @property
    def block_size_bytes(self):  # type: () -> list
        return self._get_field("BlockSizeBytes")

    @property
    def capacity(self):  # type: () -> dict
        return self._get_field("Capacity")

    @capacity.setter
    def capacity(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New capacity must be dict. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Capacity": new})
        self.reload()

    @property
    def capacity_sources(self):
        return self._get_field("CapacitySources")

    @property
    def odata_id(self):  # type: () -> str
        return self._get_field("@odata.id")

    @property
    def case_preserved(self):  # type: () -> bool
        return self._get_field("CasePreserved")

    @property
    def case_sensitive(self):  # type: () -> bool
        return self._get_field("CaseSensitive")

    @property
    def block_size_bytes(self):  # type: () -> list
        return self._get_field("BlockSizeBytes")

    @property
    def character_code_set(self):
        return self._get_field("CharacterCodeSet")

    @property
    def cluster_size_bytes(self):  # type: () -> int
        return self._get_field("ClusterSizeBytes")

    @property
    def identifier(self):
        return self._get_field("Identifier")

    @property
    def imported_share(self):
        return self._get_field("ImportedShare")

    @property
    def imported_shares(self):
        return self._get_field("ImportedShares")

    @property
    def io_statistics(self):
        return self._get_field("IOStatistics")

    def get_capacity_sources(self):
        capacity_sources = self._client.get("{0}/CapacitySources".format(self.path)).json['Members']
        for capacity_source in capacity_sources:
            capacity_sources.append(CapacitySource(client=self._client, path=capacity_source['@odata.id']))
        return capacity_sources

    def get_capacity_source(self, capacity_source_id):  # type: (str) -> Optional[Volume]
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        capacity_source = self._client.get(path).json
        return CapacitySource.from_json(self._client, capacity_source)

    def create_capacity_source(self, capacity_source_id, data):
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        self._client.post(path, body=data)

    def patch_capacity_source(self, capacity_source_id, data):
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        self._client.patch(path, body=data)

    def delete_capacity_source(self, capacity_source_id):  # type: (str) -> None
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        self._client.delete(path)
