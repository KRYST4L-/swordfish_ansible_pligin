from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Drive(SwordfishAPIObject):
    def __init__(self, client, drive_group, path):
        super(Drive, self).__init__(client, path)

    @property
    def status(self):
        return self._get_field('Status').get('Health')

    @property
    def capacity(self):
        return self._get_field('CapacityBytes')

    @property
    def model(self):
        return self._get_field('Model')

    @property
    def serial_number(self):
        return self._get_field('SerialNumber')

    @property
    def slot(self):
        return self._get_field('PhysicalLocation').get('PartLocation').get('LocationType')
