from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.client.response import HTTPClientResponse
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError, \
    RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.capacity_source import CapacitySource
import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import CAPACITY_SOURCES

try:
    from typing import List, Optional
except ImportError:
    List = None


class StoragePool(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(StoragePool, self).__init__(*args, **kwargs)

    @property
    def actions(self):  # type: () -> list
        return self._get_field("Actions")

    @property
    def odata_id(self):  # type: () -> str
        return self._get_field("@odata.id")

    @property
    def allocated_pools(self):  # type: () -> list
        return self._get_field("AllocatedPools")

    @property
    def allocated_volumes(self):  # type: () -> list
        return self._get_field("AllocatedVolumes")

    @property
    def block_size_bytes(self):  # type: () -> int
        return self._get_field("BlockSizeBytes")

    @property
    def capacity(self):  # type: () -> dict
        return self._get_field("Capacity")

    @capacity.setter
    def capacity(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New capacity must be dict. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Capacity": new})
        self.reload()

    @property
    def capacity_sources(self):
        return self._get_field("CapacitySources")

    @property
    def class_of_service(self):
        return self._get_field("ClassesOfService")

    @property
    def compressed(self):  # type: () -> bool
        return self._get_field("Compressed")

    @property
    def compression_enabled(self):  # type: () -> bool
        return self._get_field("CompressionEnabled")

    @property
    def deduplicated(self):
        return self._get_field("Deduplicated")

    @property
    def deduplication_enabled(self):
        return self._get_field("DeduplicationEnabled")

    @property
    def default_class_of_service(self):
        return self._get_field("DefaultClassOfService")

    @property
    def default_compression_behavior(self):
        return self._get_field("DefaultCompressionBehavior")

    @property
    def default_deduplication_behavior(self):
        return self._get_field("DefaultDeduplicationBehavior")

    @property
    def default_encryption_behavior(self):
        return self._get_field("DefaultEncryptionBehavior")

    @property
    def encrypted(self):
        return self._get_field("Encrypted")

    @property
    def encryption_enabled(self):
        return self._get_field("EncryptionEnabled")

    @property
    def identifier(self):
        return self._get_field("Identifier")

    @property
    def io_statistics(self):
        return self._get_field("IOStatistics")

    def get_allocated_volume(self, volume_id):  # type: (str) -> Optional[Volume]
        if not isinstance(volume_id, str):
            raise TypeError("Volume_id must be str. Received: {0}".format(type(str)))
        try:
            volume = self._client.get("{0}/AllocatedVolumes/{1}".format(self._path, volume_id)).json
            return Volume.from_json(self._client, volume)
        except RESTClientNotFoundError:
            return None

    def create_allocated_volume(self, data, volume_id):  # type: (dict, str) -> HTTPClientResponse
        if not isinstance(volume_id, str):
            raise TypeError("Volume_id must be str. Received: {0}".format(type(str)))
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        path = "{0}/AllocatedVolumes/{1}".format(self._path, volume_id)
        return self._client.post(path, body=data)

    def patch_allocated_volume(self, data, volume_id):  # type: (dict, str) -> HTTPClientResponse
        if not isinstance(volume_id, str):
            raise TypeError("Volume_id must be str. Received: {0}".format(type(str)))
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        path = "{0}/AllocatedVolumes/{1}".format(self._path, volume_id)
        return self._client.patch(path, body=data)

    def delete_allocated_volume(self, volume_id):  # type: (str) -> None
        if not isinstance(volume_id, str):
            raise TypeError("Volume_id must be string. Received: {0}".format(type(volume_id)))
        path = "{0}/AllocatedVolumes/{1}".format(self._path, volume_id)
        self._client.delete(path)

    def get_capacity_sources(self):
        capacity_sources = []
        if not self._data.get("CapacitySources"):
            return capacity_sources

        capacity_sources_data = self._client.get("{0}/CapacitySources".format(self.path)).json['Members']

        for capacity_source in capacity_sources_data:
            capacity_sources.append(CapacitySource(client=self._client, path=capacity_source['@odata.id']))
        return capacity_sources

    def get_capacity_source(self, capacity_source_id):  # type: (str) -> Optional[Volume]
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        capacity_source = self._client.get(path).json
        return CapacitySource.from_json(self._client, capacity_source)

    def create_capacity_source(self, capacity_source_id, data):
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        self._client.post(path, body=data)

    def patch_capacity_source(self, capacity_source_id, data):
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        self._client.patch(path, body=data)

    def delete_capacity_source(self, capacity_source_id):
        path = "{0}/CapacitySources/{1}".format(self.path, capacity_source_id)
        self._client.delete(path)
