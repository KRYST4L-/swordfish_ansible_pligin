from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Optional, ClassVar
except ImportError:
    Optional = ClassVar = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Account(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Account, self).__init__(*args, **kwargs)

    @property
    def username(self):  # type: () -> str
        return self._get_field("UserName")

    @property
    def locked(self):  # type: () -> bool
        return self._get_field("Locked")

    @property
    def role_id(self):  # type: () -> str
        return self._get_field("RoleId")

    @role_id.setter
    def role_id(self, role_id):  # type: (str) -> None
        if not isinstance(role_id, str):
            raise TypeError("Role id must be string. Received: {0}".format(type(role_id)))
        self._client.patch(self._path, body={"RoleId": role_id})
        self.reload()

    @property
    def enabled(self):  # type: () -> bool
        return self._get_field("Enabled")

    @enabled.setter
    def enabled(self, enabled):  # type: (bool) -> None
        if not isinstance(enabled, bool):
            raise TypeError("Enabled must be boolean. Received: {0}".format(type(enabled)))
        self._client.patch(self._path, body={"Enabled": enabled})
        self.reload()

    def set_password(self, password):  # type: (str) -> None
        if not isinstance(password, str):
            raise TypeError("Password must be string. Received: {0}".format(type(password)))
        self._client.patch(self._path, body={"Password": password})
