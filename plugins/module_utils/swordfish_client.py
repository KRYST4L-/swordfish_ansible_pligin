import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_ENDPOINT, ROOT_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.account.service import AccountService
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.fabric import Fabric
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.manager.manager import Manager
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.drive_group import DriveGroup
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_controller import \
    StorageController
from ansible_collections.spbstu.swordfish.plugins.module_utils.rest_client import RESTClient


class SwordfishAPI(RESTClient):

    def __init__(
            self,
            base_url,
            username=None,
            password=None,
            validate_certs=False,
            timeout=60,
    ):
        super(SwordfishAPI, self).__init__(base_url=base_url, username=username, password=password,
                                           validate_certs=validate_certs, timeout=timeout)

    def get_drive_groups(self):  # type: () -> List[DriveGroup]
        rv = []
        drive_groups_data = self.get(STORAGE_ENDPOINT).json['Members']
        for data in drive_groups_data:
            rv.append(DriveGroup(client=self, path=data['@odata.id']))
        return rv

    def get_manager_collection(self):  # type: () -> List[Manager]
        managers = []
        managers_collection = self.get("{0}/Managers".format(ROOT_ENDPOINT)).json
        for member in managers_collection["Members"]:
            manager_data = self.get(member["@odata.id"]).json
            managers.append(Manager.from_json(self, manager_data))
        return managers

    def get_manager(self, manager_id):  # type: (str) -> Optional[Manager]
        if not isinstance(manager_id, str):
            raise TypeError("Manager id must be string. Received: {0}".format(type(manager_id)))

        manager_path = "{0}/Managers/{1}".format(ROOT_ENDPOINT, manager_id)
        try:
            manager_data = self.get(manager_path).json
        except RESTClientNotFoundError:
            return None
        return Manager.from_json(self, manager_data)

    def get_storage(self, storage_id):  # type: (str) -> Optional[Storage]
        if not isinstance(storage_id, str):
            raise TypeError("Storage id must be string. Received: {0}".format(type(storage_id)))
        storage_path = "{0}/Storage/{1}".format(ROOT_ENDPOINT, storage_id)
        try:
            storage_data = self.get(storage_path).json
        except RESTClientNotFoundError:
            return None
        return Storage.from_json(self, storage_data)

    def get_fabric(self, fabric_id):  # type: (str) -> Optional[Fabric]
        if not isinstance(fabric_id, str):
            raise TypeError("Fabric id must be string. Received: {0}".format(type(fabric_id)))
        fabric_path = "{0}/Fabrics/{1}".format(ROOT_ENDPOINT, fabric_id)
        try:
            fabric_data = self.get(fabric_path).json
        except RESTClientNotFoundError:
            return None
        return Fabric.from_json(self, fabric_data)

    def get_storage_controller(self, storage_id, storage_ctrl_id):  # type: (str, str) -> Optional[StorageController]
        if not isinstance(storage_id, str) or not isinstance(storage_ctrl_id, str):
            raise TypeError("Storage_id and storage_ctrl_id must be string. Received: {0} and {1}"
                            .format(type(storage_id), type(storage_ctrl_id)))
        try:
            data = self.get("{0}/Storage/{1}/Controllers/{2}".format(ROOT_ENDPOINT, storage_id, storage_ctrl_id)).json
            return StorageController.from_json(self, data)
        except RESTClientNotFoundError:
            return None

    def get_account_service(self):  # type: () -> AccountService
        account_service_path = "{0}/AccountService".format(ROOT_ENDPOINT)
        try:
            account_service_data = self.get(account_service_path).json
        except RESTClientNotFoundError:
            return None
        return AccountService.from_json(self, account_service_data)
