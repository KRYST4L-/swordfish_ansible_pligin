def build_url(*parts):
    return '/'.join(parts)


VERSION1 = 'v1'
REDFISH = 'redfish'
MANAGERS = 'Managers'
NETWORK_PROTOCOL = 'NetworkProtocol'
VOLUMES = 'Volumes'
PROVIDING_VOLUMES = 'ProvidingVolumes'
CAPACITY_SOURCES = 'CapacitySources'
STORAGE = 'Storage'
STORAGE_POOLS = 'StoragePools'
PROVIDING_POOLS = 'ProvidingPools'
ROLES = 'Roles'
ACCOUNTS = 'Accounts'
ACCOUNT_SERVICE = 'AccountService'
FABRICS = "Fabrics"
FILE_SYSTEM = "FileSystems"

ROOT_ENDPOINT = build_url(REDFISH, VERSION1)
CHASSIS_ENDPOINT = build_url(ROOT_ENDPOINT, "Chassis")

SESSION_SERVICE_ENDPOINT = build_url(ROOT_ENDPOINT, "SessionService")
SESSION_ENDPOINT = build_url(SESSION_SERVICE_ENDPOINT, "Sessions")

STORAGE_ENDPOINT = build_url(ROOT_ENDPOINT, STORAGE)
STORAGE_POOLS_ENDPOINT = build_url("{path_to_storage}", STORAGE_POOLS)
STORAGE_VOLUMES_ENDPOINT = build_url(STORAGE_ENDPOINT, "{storage_id}", VOLUMES)
STORAGE_POOL_CAPACITY_SOURCES_ENDPOINT = build_url(STORAGE_POOLS_ENDPOINT, "{storage_pool_id}", CAPACITY_SOURCES)
VOLUME_CAPACITY_SOURCES_ENDPOINT = build_url(STORAGE_VOLUMES_ENDPOINT, "{volume_id}", CAPACITY_SOURCES)
# MY_STORAGE_VOLUMES_ENDPOINT = build_url("{storage_id}", VOLUMES)

NETWORK_PROTOCOL_ENDPOINT = build_url(ROOT_ENDPOINT, MANAGERS, "{managerID}", NETWORK_PROTOCOL)
NAMESPACE_ENDPOINT = build_url(STORAGE_ENDPOINT, "{storage_id}", VOLUMES, "{namespace}")
FABRICS_ENDPOINT = build_url(ROOT_ENDPOINT, FABRICS)

ACCOUNT_SERVICE_ENDPOINT = build_url(ROOT_ENDPOINT, ACCOUNT_SERVICE)
ACCOUNTS_ENDPOINT = build_url(ACCOUNT_SERVICE_ENDPOINT, ACCOUNTS)
ROLES_ENDPOINT = build_url(ACCOUNT_SERVICE_ENDPOINT, ROLES)
SIM_ACCOUNT_ENDPOINT = build_url(ACCOUNTS_ENDPOINT, "{account}")
SIM_ROLE_ENDPOINT = build_url(ROLES_ENDPOINT, "{role}")

FILE_SYSTEM_ENDPOINT = build_url("{path_to_file_system}", FILE_SYSTEM, "{file_system}")
