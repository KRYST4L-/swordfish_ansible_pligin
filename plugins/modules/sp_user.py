#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from functools import partial

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

DOCUMENTATION = r'''
---
module: sp_user
short_description: Manage SP accounts
version_added: "1.0.0"
description:
  ---
options:
  username:
    required: True
    type: str
    description: User name of account
  password:
    type: str
    description: Password of user account/Required if new user
  role:
    type: str
    choices: [DevOps, CloudAdmin, StorageAdmin, NoAccess]
    description: Actors roles with various level of permissions
  enabled:
    type: bool
    description: Indication of account enabled/disabled
  state:
    type: str
    choices: [present, absent]
    default: present
    description: absent parameter deletes an existing account
'''

EXAMPLES = r'''
---
- name: Creating
  sp_user:
    username: "TestUser"
    password: "TestPassword"
    role: "DevOps"
    enabled: true
    state: "present"
- name: Editing
  sp_user:
    username: "TestUser"
    enabled: false
    role: "NoAccess"
- name: Deleting
  sp_user:
    username: "TestUser"
    state: "absent"
'''

RETURN = r'''
---
msg:
  type: str
  returned: always
  description: Operation status
error:
  type: str
  returned: on error
  description: Error details
'''


class UserModule(SwordfishModule):
    def __init__(self):
        argument_spec = {
            "username": {"type": "str", "required": True},
            "password": {"type": "str", "required": False, "no_log": True},
            "role": {
                "type": "str",
                "required": False,
                "choices": ["Administrator", "DevOps", "CloudAdmin", "StorageAdmin", "NoAccess"]
            },
            "enabled": {"type": "bool", "required": False},
            "state": {
                "type": "str",
                "required": False,
                "default": "present",
                "choices": ["present", "absent"]
            },
        }
        super(UserModule, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        changes = []
        account_service = self.client.get_account_service()
        account = account_service.get_account(self.params["username"])
        if self.params["state"] == "present":
            if account:
                # Account exists, update it if necessary
                if self.params["password"] is not None:
                    changes.append(lambda: account_service.set_password(self.params["password"]))
                if self.params["role"] and self.params["role"] != account.role_id:
                    changes.append(lambda: setattr(account, 'role_id', self.params["role"]))
                if self.params["enabled"] is not None and self.params["enabled"] != account.enabled:
                    changes.append(lambda: setattr(account, 'enabled', self.params["enabled"]))
            else:
                changes.append(partial(
                    account_service.create_account,
                    self.params["username"],
                    self.params["password"],
                    self.params["role"],
                    self.params["enabled"],
                ))
            if changes and not self.check_mode:
                for action in changes:
                    action()
                self.exit_json(msg="Operation successful.", changed=True)
            else:
                self.exit_json(msg="No changes required.", changed=False)
        else:
            # Delete the account if it exists
            if account:
                changes.append(lambda: account_service.delete_account(self.params["username"]))
        if changes and not self.check_mode:
            for action in changes:
                action()
            self.exit_json(msg="Successful!", changed=True)
        else:
            self.exit_json(msg="No changes required!", changed=False)


def main():
    UserModule()


if __name__ == "__main__":
    main()
