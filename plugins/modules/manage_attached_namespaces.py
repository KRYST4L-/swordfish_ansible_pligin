#!/usr/bin/env python3
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from functools import partial

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

DOCUMENTATION = r'''
module: manage_attached_namespaces
short_description: Attach/Detach a Namespace
description:
  - This module is intended to attach or detach a Namespace
   to an IO Controller to make it visible to the hosts
   that connect to that IO Conttoller, and accessible for block storage operations
options:
  storage_id:
    type: str
    required: True
  storage_controller_id:
    type: str
    required: True
  namespace_urls:
    type: list
    elements: str
    required: True
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) attaches a namespaces if I(namespace) does not attached.
      - C(absent) detached an attached namespace.
'''

EXAMPLES = r'''
- name: Test attach namespace | Attach a namespace
  spbstu.swordfish.manage_attached_namespaces:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    storage_controller_id: "NVMeIOController"
    state: "present"
    namespace_urls:
      - /redfish/v1/Storage/IPAttachedDrive1/Volumes/SimpleNamespace1
'''

RETURN = r'''
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
id:
  type: str
  returned: on success
  description: Storage controller id
name:
  type: str
  returned: on success
  description: Storage controller name
path:
  type: str
  returned: on success
  description: Storage controller path
description:
  type: str
  returned: on success
  description: Storage controller description
status:
  type: str
  returned: on success
  description: Storage controller status
manufacturer:
  type: str
  returned: on success
  description: Storage controller manufacturer
model:
  type: str
  returned: on success
  description: Storage controller model
serial_number:
  type: str
  returned: on success
  description: Storage controller serial number
part_number:
  type: str
  returned: on success
  description: Storage controller part number
firmware_version:
  type: str
  returned: on success
  description: Storage controller firmware version
supported_controller_protocols:
  type: list
  elements: str
  returned: on success
  description: Storage controller supported controller protocols
supported_device_protocols:
  type: list
  elements: str
  returned: on success
  description: Storage controller supported device protocols
nvme_controller_properties:
    type: list
    elements: str
    returned: on success
    description: Storage controller nvme controller properties
links:
  type: list
  elements: str
  returned: on success
  description: Storage controller links
allocated_bytes:
  type: int
  returned: on success
consumed_bytes:
  type: int
  returned: on success
'''


class ManageAttachedNamespaces(SwordfishModule):
    def __init__(self):
        argument_spec = {
            'storage_id': {
                'type': 'str',
                'required': True,
            },
            'storage_controller_id': {
                'type': 'str',
                'required': True,
            },
            'namespace_urls': {
                'type': 'list',
                'elements': 'str',
                'required': True
            },
            "state": {
                "type": "str",
                "default": "present",
                "choices": ["present", "absent"],
                "required": False
            },
        }
        super(ManageAttachedNamespaces, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        storage_controller = self.client.get_storage_controller(self.params["storage_id"],
                                                                self.params["storage_controller_id"])
        if storage_controller is None:
            self.fail_json(
                msg="Operation failed while finding storage controller.",
                error="Storage controller '{0}' is not exists.".format(
                    self.params["storage_controller_id"])
            )
            return
        msg = "Operation successful."
        changed = True
        action = partial(storage_controller.attach_namespace, self.params["namespace_urls"])
        if self.params["state"] == "absent":
            action = partial(storage_controller.detach_namespace, self.params["namespace_urls"])
        try:
            if not self.check_mode:
                action()
            else:
                changed = False
                msg = "No changes required."
        except Exception as e:
            self.fail_json(
                msg="Operation failed while updating storage controller.",
                error=str(e)
            )
            return
        self.exit_json(
            msg=msg,
            changed=changed,
            id=storage_controller.id,
            name=storage_controller.name,
            path=storage_controller.path,
            description=storage_controller.description,
            status=storage_controller.status,
            manufacturer=storage_controller.manufacturer,
            model=storage_controller.model,
            serial_number=storage_controller.serial_number,
            part_number=storage_controller.part_number,
            firmware_version=storage_controller.firmware_version,
            supported_controller_protocols=storage_controller.supported_controller_protocols,
            supported_device_protocols=storage_controller.supported_device_protocols,
            nvme_controller_properties=storage_controller.nvme_controller_properties,
            links=storage_controller.links,
        )


def main():
    ManageAttachedNamespaces()


if __name__ == '__main__':
    main()
