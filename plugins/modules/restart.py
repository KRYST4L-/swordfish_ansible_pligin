#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r'''
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class RestartModule(SwordfishModule):

    def __init__(self):
        argument_spec = \
            {
                "force":
                    {
                        "required": False,
                        "type": "bool",
                        "default": False
                    }
            }
        super(RestartModule, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        manager = self.client.get_manager("EBOF-SoC")

        if self.params["force"]:
            manager.reset_force()
        else:
            manager.reset_graceful()

        self.exit_json(
            msg="Operation successful.",
            changed=False,
        )


def main():
    RestartModule()


if __name__ == '__main__':
    main()
