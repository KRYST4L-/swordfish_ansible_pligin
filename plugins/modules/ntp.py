from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r"""
---
module: ntp
short_description: Configures NTP servers.
description:
  - Configures NTP servers.
  - This module supports check mode.
options:
  manager_id:
    type: str
    required: True
  ntp_enabled:
    required: false
    type: bool
    description: Indicates if NTP protocol is enabled or disabled. If disabled host time will be used.
  ntp_servers:
    required: false
    type: list
    description:
      - List of NTP servers IP.
      - If empty list is set, all servers configuration will be removed.
    elements: str
"""

RETURN = r"""
---
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
"""

EXAMPLES = r"""
---
- name: Test NTP configuration | Update NTP servers
  spbstu.swordfish.ntp:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    ntp_servers:
      - 192.168.1.11
      - example.com
      - 127.0.0.1
    manager_id: "EBOF-SoC"

- name: Test NTP configuration | Test NTP servers updated
  spbstu.swordfish.ntp:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    ntp_servers:
      - 192.168.1.11
      - example.com
      - 127.0.0.1
    manager_id: "EBOF-SoC"
  register: result

- name: Test NTP server removing | Remove NTP server
  spbstu.swordfish.ntp:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    ntp_servers:
      - 127.0.0.1
    manager_id: "EBOF-SoC"
  register: result

- name: Test NTP servers removing | Remove all NTP servers
  spbstu.swordfish.ntp:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    manager_id: "EBOF-SoC"
    ntp_servers: []
  register: result

- name: Test NTP configuration | Disable NTP support
  spbstu.swordfish.ntp:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    manager_id: "EBOF-SoC"
    ntp_enabled: false
  register: result
"""

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class Ntp(SwordfishModule):

    def __init__(self):
        argument_spec = {
            "manager_id": {"type": "str", "required": True},
            "ntp_enabled": {"type": "bool", "required": False},
            "ntp_servers": {"type": "list", "required": False, "elements": "str"},
        }
        super(Ntp, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        actions = []

        manager = self.client.get_manager(self.params["manager_id"])
        network_protocol = manager.network_protocol

        if (self.params["ntp_enabled"] is not None and (not network_protocol.contains_field("NTP") or
                                                        network_protocol.ntp.get("ProtocolEnabled") !=
                                                        self.params.get("ntp_enabled"))):
            actions.append(lambda: setattr(
                network_protocol,
                "ntp_enabled",
                self.params["ntp_enabled"],
            ))

        if (self.params["ntp_servers"] is not None and (not network_protocol.contains_field("NTP") or
                                                        network_protocol.ntp.get("NTPServers") !=
                                                        self.params.get("ntp_servers"))):
            actions.append(lambda: setattr(
                network_protocol,
                "ntp_servers",
                self.params["ntp_servers"],
            ))

        if actions:
            if not self.check_mode:
                for action in actions:
                    action()
            self.exit_json(msg="Operation successful.", changed=True)
        else:
            self.exit_json(msg="No changes required.", changed=False)


def main():
    Ntp()


if __name__ == "__main__":
    main()
