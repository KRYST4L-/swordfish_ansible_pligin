from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r"""
---
module: storage_pools_info
short_description: Get information about configured pools.
"""

RETURN = r"""
{
    "changed": true,
    "invocation": {
        "module_args": {
            "body": {
                "Description": "Endpoint",
                "EndpointProtocol": "NVMeOverFabrics"
            },
            "connection": {
                "base_url": "https://localhost:5000",
                "password": "VALUE_SPECIFIED_IN_NO_LOG_PARAMETER",
                "username": "Administrator"
            },
            "endpoint_id": "TestEndpoint",
            "endpoints_collection_path": "/redfish/v1/Fabrics/NVMeoF/Endpoints",
            "state": "present"
        }
    },
    "msg": "Operation successful.",
    "resp": {
        "@odata.id": "/redfish/v1/Fabrics/NVMeoF/Endpoints/TestEndpoint",
        "@odata.type": "#Endpoint.vTestEndpoint_7_NVMeoF.Endpoint",
        "Description": "Endpoint",
        "EndpointProtocol": "NVMeOverFabrics",
        "Id": "TestEndpoint",
        "Name": "Endpoint"
    }
}
"""

EXAMPLES = r"""
- name: Test manage endpoints | Delete Test Endpoint
  spbstu.swordfish.manage_endpoints:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    endpoints_collection_path: "/redfish/v1/Fabrics/NVMeoF/Endpoints"
    endpoint_id: "TestEndpoint"
    state: "absent"
- name: Test manage endpoints | Check state
  assert:
    that: "result.changed == true"
"""

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage


class StoragePoolsInfo(SwordfishModule):
    def __init__(self):
        super(StoragePoolsInfo, self).__init__(
            supports_check_mode=True
        )

    def run(self):
        storage_members = self.client.get(STORAGE_ENDPOINT).json.get("Members")
        pools = []
        for member in storage_members:
            storage = Storage(client=self.client, path=member['@odata.id'])
            pools.append(
                {
                    'storage': storage.path,
                    'storage_pools': [{
                        '@odata.id': pool.odata_id,
                        'name': pool.name,
                        'id': pool.id,
                        'description': pool.description,
                    } for pool in storage.storage_pools]
                }
            )
        self.exit_json(
            msg="Operation successful.",
            pools_info=pools,
            changed=False
        )


def main():
    StoragePoolsInfo()


if __name__ == "__main__":
    main()
