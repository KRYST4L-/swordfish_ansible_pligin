from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from functools import partial
import json
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError, RESTClientNotFoundError

DOCUMENTATION = r"""
---
module: capacity_source
short_description: Represent the source and type of storage capacity.
description:
  - This module supports check mode.
options:
  storage_id:
    type: str
    required: True
  storage_pool_id:
    type: str
    required: False
  volume_id:
    type: str
    required: False
  file_system_id:
    type: str
    required: False
  name:
    type: str
    required: True
  description:
    type: str
    required: False
  is_thin_provisioned:
    type: bool
    required: False
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) creates a new volume if I(storage_id) does not exists.
      - C(absent) deletes an existing volume.
"""

RETURN = r"""
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
"""

EXAMPLES = r"""
- name: Create new capacity source
  spbstu.swordfish.capacity_source:
    connection:
      base_url: "https://127.0.0.1:5000"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    storage_pool_id: "my_pool"
    name: "my_capacity_source"
    description: "some_description"
  register: result

- name: Test capacity source | Remove capacity source
  spbstu.swordfish.capacity_source:
    connection:
      base_url: "https://127.0.0.1:5000"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    storage_pool_id: "my_pool"
    name: "my_capacity_source"
    state: "absent"
  register: result
"""


class CapacitySource(SwordfishModule):
    def __init__(self):
        argument_spec = {
            "storage_id": {
                "type": "str",
                "required": True
            },
            "storage_pool_id": {
                "type": "str",
                "required": False
            },
            "volume_id": {
                "type": "str",
                "required": False
            },
            "file_system_id": {
                "type": "str",
                "required": False
            },
            "description": {
                "type": "str",
                "required": False
            },
            "name": {
                "type": "str",
                "required": True
            },
            "is_thin_provisioned": {
                "type": "bool",
                "required": False
            },
            "state": {
                "type": "str",
                "default": "present",
                "choices": ["present", "absent"],
                "required": False
            },
        }
        super(CapacitySource, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True,
        )

    def run(self):
        resource = self.client.get_storage(self.params["storage_id"])
        storage_pool_id_provided = self.params["storage_pool_id"] is not None
        volume_id_provided = self.params["volume_id"] is not None
        file_system_id_provided = self.params["file_system_id"] is not None

        if storage_pool_id_provided:
            resource = resource.get_storage_pool(self.params["storage_pool_id"])
        elif volume_id_provided:
            resource = resource.get_volume(self.params["volume_id"])
        elif file_system_id_provided:
            resource = resource.get_volume(self.params["file_system_id"])
        else:
            self.exit_json(msg="No provided resource.", changed=False)

        action = []
        data = dict()
        capacity_source = None
        try:
            capacity_source = resource.get_capacity_source(self.params["name"])
        except (RESTClientError, RESTClientNotFoundError) as e:
            pass

        if self.params["state"] == "present":
            data["Name"] = self.params["name"]
            if self.params["description"] is not None:
                data["Description"] = self.params["description"]
            if self.params["is_thin_provisioned"] is not None:
                data["IsThinProvisioned"] = self.params["is_thin_provisioned"]

            if capacity_source:
                if capacity_source.description != data["Description"]:
                    action.append(
                        partial(resource.patch_capacity_source, self.params["name"], json.loads(json.dumps(data))))
            else:
                action.append(
                    partial(resource.create_capacity_source, self.params["name"], json.loads(json.dumps(data))))
        else:
            if capacity_source:
                action.append(partial(resource.delete_capacity_source, self.params["name"]))

        if not self.check_mode:
            for item in action:
                item()

        if action:
            self.exit_json(msg="Operation successful.", changed=True)
        else:
            self.exit_json(msg="No changes required.", changed=False)


def main():
    CapacitySource()


if __name__ == "__main__":
    main()
