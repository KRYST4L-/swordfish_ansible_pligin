from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import copy
import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError

DOCUMENTATION = r"""
module: storage_pool
short_description: Create a storage pool containing an amount of provisioned bytes.
description:
  - This module supports check mode.
options:
  storage_id:
    type: str
    required: True
  description:
    type: str
    required: False
  name:
    type: str
    required: True
  is_thin_provisioned:
    type: bool
    required: False
  provisioned_bytes:
    type: int
    required: False
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) creates a new storage pool if I(storage_id) does not exists.
      - C(absent) deletes an existing storage pool.
"""

RETURN = r"""
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
"""

EXAMPLES = r"""
- name: Test storage_pool creation | Create StoragePoolTest
  spbstu.swordfish.storage_pool:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "StoragePoolTest"
    provisioned_bytes: 10000
    description: "Description"
    is_thin_provisioned: false

- name: Test storage_pool changing | Change description of StoragePoolTest
  spbstu.swordfish.storage_pool:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "StoragePoolTest"
    description: "AnotherDescription"

- name: Test storage_pool changing | Change provisioned_bytes of StoragePoolTest
  spbstu.swordfish.storage_pool:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "StoragePoolTest"
    description: "AnotherDescription"
    provisioned_bytes: 15000

- name: Test storage_pool changing | Change is_thin_provisioned of StoragePoolTest
  spbstu.swordfish.storage_pool:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "StoragePoolTest"
    description: "AnotherDescription"
    provisioned_bytes: 15000
    is_thin_provisioned: true

- name: Test storage_pool deleting | Delete StoragePoolTest
  spbstu.swordfish.storage_pool:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "StoragePoolTest"
    state: absent
"""

from functools import partial
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class StoragePool(SwordfishModule):

    def __init__(self):
        argument_spec = {
            "storage_id": {"type": "str", "required": True},
            "description": {"type": "str", "required": False},
            "name": {"type": "str", "required": True},
            "is_thin_provisioned": {"type": "bool", "required": False},
            "provisioned_bytes": {"type": "int", "required": False},
            "state": {"type": "str", "default": "present", "choices": ["present", "absent"], "required": False},
        }
        super(StoragePool, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True,
        )

    def run(self):
        storage = self.client.get_storage(self.params["storage_id"])
        action = []
        data = dict()
        storage_pool = storage.get_storage_pool(self.params["name"])
        if self.params["state"] == "present":
            data["Name"] = self.params["name"]
            if self.params["description"] is not None:
                data["Description"] = self.params["description"]
            if self.params["is_thin_provisioned"] is not None or self.params["provisioned_bytes"] is not None:
                capacity_body = dict()
                if self.params["is_thin_provisioned"] is not None:
                    capacity_body["IsThinProvisioned"] = self.params["is_thin_provisioned"]
                if self.params["provisioned_bytes"] is not None:
                    capacity_data = dict()
                    capacity_data["ProvisionedBytes"] = self.params["provisioned_bytes"]
                    capacity_body["Data"] = capacity_data
                data["Capacity"] = capacity_body
            if storage_pool:
                if ("Description" in data and
                        (not storage_pool.contains_field("Description") or
                         storage_pool.contains_field("Description") and
                         data["Description"] != storage_pool.description)):
                    action.append(lambda: setattr(storage_pool, "description", data["Description"]))
                if "Capacity" in data:
                    try:
                        old_capacity = copy.deepcopy(storage_pool.capacity)
                        if ("IsThinProvisioned" in data["Capacity"] and data["Capacity"]["IsThinProvisioned"] !=
                                old_capacity.get("IsThinProvisioned")):
                            old_capacity["IsThinProvisioned"] = data["Capacity"]["IsThinProvisioned"]
                        if ("Data" in old_capacity and data["Capacity"]["Data"]["ProvisionedBytes"] !=
                                old_capacity["Data"].get("ProvisionedBytes")):
                            old_capacity["Data"]["ProvisionedBytes"] = data["Capacity"]["Data"][
                                "ProvisionedBytes"]
                        else:
                            old_capacity["Data"] = {
                                "ProvisionedBytes": data["Capacity"]["Data"]["ProvisionedBytes"]
                            }
                        if old_capacity != storage_pool.capacity:
                            action.append(lambda: setattr(storage_pool, "capacity", old_capacity))
                    except SwordfishFieldNotFoundError:
                        action.append(lambda: setattr(storage_pool, "capacity", data["Capacity"]))
            else:
                action.append(partial(storage.create_storage_pool, json.loads(json.dumps(data)), self.params["name"]))
        else:
            if storage_pool:
                action.append(partial(storage.delete_storage_pool, self.params["name"]))

        if not self.check_mode:
            for item in action:
                item()
        if action:
            self.exit_json(msg="Operation successful.", changed=True)
        else:
            self.exit_json(msg="No changes required.", changed=False)


def main():
    StoragePool()


if __name__ == "__main__":
    main()
