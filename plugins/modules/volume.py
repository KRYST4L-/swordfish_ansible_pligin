from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from functools import partial

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError, RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class Volume(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'required': False,
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'volume_id': {
                'type': 'str',
                'required': False,
            },
            'volume_collection_id': {
                'type': 'str',
                'required': True,
            },
            "storage_id": {"type": "str", "required": True},
            'body': {
                'type': 'dict',
                'required': False,
                'default': {
                    'Manufacturer': 'NVMeDriveVendorFoo'
                },
            }
        }
        super(Volume, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        action = None
        volume_exists = False
        has_id = self.params['volume_id'] is not None

        storage = self.client.get_storage(self.params["storage_id"])
        volume_collection = storage.get_volume_collection(self.params["volume_collection_id"])
        if has_id:
            try:
                volume_collection.get_resource(self.params["volume_id"])
                volume_exists = True
            except (RESTClientError, RESTClientNotFoundError) as e:
                pass

        if self.params['state'] == 'present':
            if volume_exists:
                if self.params['body']:
                    action = partial(volume_collection.patch_resource, resource_id=self.params['volume_id'],
                                     data=self.params['body'])
            else:
                action = partial(volume_collection.create_resource, resource_id=self.params['volume_id'],
                                 data=self.params['body'])
        else:  # absent
            if volume_exists:
                action = partial(volume_collection.delete_resource, resource_id=self.params['volume_id'])

        if not action:
            self.exit_json(msg='No changes required', changed=False)

        resp = None
        if not self.check_mode:
            resp = action().json

        self.exit_json(
            msg="Operation successful.",
            resp=resp,
            changed=True,
        )


def main():
    Volume()


if __name__ == "__main__":
    main()
