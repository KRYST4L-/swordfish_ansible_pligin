from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

DOCUMENTATION = r"""
module: storage_pool_volume
short_description: Creates a volume in the allocated volumes of an existing storage pool.
description:
  - This module supports check mode.
options:
  storage_id:
    type: str
    required: True
  storage_pool_id:
    type: str
    required: True
  name:
    type: str
    required: True
  description:
    type: str
    required: False
  capacity_bytes:
    type: int
    required: False
  raid_type:
    type: str
    required: False
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) creates a new volume if I(storage_id) does not exists.
      - C(absent) deletes an existing volume.
"""

RETURN = r"""
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
"""

EXAMPLES = r"""
- name: Test volume creating | Create VolumeTest
  spbstu.swordfish.storage_pool_volume:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    storage_pool_id: "BasePool"
    name: "VolumeTest"

- name: Test volume changing | Change VolumeTest
  spbstu.swordfish.storage_pool_volume:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    storage_pool_id: "BasePool"
    name: "VolumeTest"
    description: "Description"
    capacity_bytes: 10000

- name: Test volume deleting | Delete VolumeTest
  spbstu.swordfish.storage_pool_volume:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    storage_pool_id: "BasePool"
    name: "VolumeTest"
    state: absent
"""

from functools import partial
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class StoragePoolVolume(SwordfishModule):

    def __init__(self):
        argument_spec = {
            "storage_id": {"type": "str", "required": True},
            "storage_pool_id": {"type": "str", "required": True},
            "description": {"type": "str", "required": False},
            "name": {"type": "str", "required": True},
            "capacity_bytes": {"type": "int", "required": False},
            "raid_type": {"type": "str", "required": False},
            "state": {"type": "str", "default": "present", "choices": ["present", "absent"], "required": False},
        }
        super(StoragePoolVolume, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        storage_pool = self.client.get_storage(self.params["storage_id"]).get_storage_pool(
            self.params["storage_pool_id"])
        volume = storage_pool.get_allocated_volume(self.params["name"])
        action = []
        if self.params["state"] == "present":
            data = dict()
            data["Name"] = self.params["name"]
            if self.params["description"] is not None:
                data["Description"] = self.params["description"]
            if self.params["capacity_bytes"] is not None:
                data["CapacityBytes"] = self.params["capacity_bytes"]
            if self.params["raid_type"] is not None:
                data["RAIDType"] = self.params["raid_type"]
            if volume:
                if ("Description" in data and
                        (not volume.contains_field("Description") or
                         volume.contains_field("Description") and data["Description"] != volume.description)):
                    action.append(lambda: setattr(volume, "description", data["Description"]))
                if ("CapacityBytes" in data and
                        (not volume.contains_field("CapacityBytes") or volume.contains_field("CapacityBytes") and
                         data["CapacityBytes"] != volume.capacity_bytes)):
                    action.append(lambda: setattr(volume, "capacity_bytes", data["CapacityBytes"]))
                if ("RAIDType" in data and
                        (not volume.contains_field("RAIDType") or volume.contains_field("RAIDType") and
                         data["RAIDType"] != volume.raid_type)):
                    action.append(lambda: setattr(volume, "raid_type", data["RAIDType"]))
            else:
                action.append(
                    partial(storage_pool.create_allocated_volume, json.loads(json.dumps(data)), self.params["name"]))
        else:
            if volume:
                action.append(partial(storage_pool.delete_allocated_volume, self.params["name"]))

        if not self.check_mode:
            for item in action:
                item()
        if action:
            self.exit_json(msg="Operation successful.", changed=True)
        else:
            self.exit_json(msg="No changes required.", changed=False)


def main():
    StoragePoolVolume()


if __name__ == "__main__":
    main()
