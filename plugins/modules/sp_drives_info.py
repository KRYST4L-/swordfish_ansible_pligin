#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r'''
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class DrivesInfoModule(SwordfishModule):

    def __init__(self):
        super(DrivesInfoModule, self).__init__(
            supports_check_mode=True
        )

    def run(self):
        drives_groups_info = []
        drive_groups = self.client.get_drive_groups()
        for group in drive_groups:
            drives_groups_info.append({
                'group_name': group.name,
                'status': group.status,
                'capacity_total': group.capacity_total,
                'capacity_used': group.capacity_used,
                'capacity_available': group.capacity_available,
                'drive_capacity': group.drive_capacity,
                'drives_total': group.drives_total,
                'drives_available': group.drives_available,
                'drives_failed': group.drives_failed,
                'drives': [dict(
                    model=drive.model,
                    serial_number=drive.serial_number,
                    status=drive.status,
                    capacity=drive.capacity,
                    slot=drive.slot
                ) for drive in group.drives]
            })
        self.exit_json(
            msg="Operation successful.",
            drives_info=drives_groups_info,
            changed=False,
        )


def main():
    DrivesInfoModule()


if __name__ == "__main__":
    main()
